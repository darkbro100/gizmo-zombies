package me.mario.zs;

import com.sk89q.worldedit.WorldEdit;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.biteeffects.BiteEffectManager;
import me.mario.zs.commands.CommandManager;
import me.mario.zs.listeners.*;
import me.mario.zs.perks.PerkManager;
import me.mario.zs.tasks.*;
import me.mario.zs.util.*;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class Main extends JavaPlugin {

    private static Main instance;
    public static Economy econ = null;
    public static WeightedRandomizer<ZombieItem> drops = new WeightedRandomizer<>();
    public static ItemStack ZOMBIE_FINDER = new ItemBuilder(Material.COMPASS).setName("\247c\247lZOMBIE LOCATOR").build();
    public static int moneyPerKill;
    public static int bossHealth;
    public static double pigZombieDamageMult;
    public static double tankZombieDamageMult;
    public static double tankZombieReductionMult;
    public static int pigZombieSpawn;
    public static int tankZombieSpawn;
    public static int babyZombieWave;
    public static int wolfWave;
    public static int speedyZombieSpawn;
    public static int damageMulitplier;
    private int tick = 0;
    private static FileHandler fh;
    private static ZombieMessage msg;

    @Override
    public void onEnable() {
        instance = this;
        fh = new FileHandler();
        saveDefaultConfig();
        msg = new ZombieMessage(this);

        moneyPerKill = getConfig().getInt("money-per-kill");
        bossHealth = getConfig().getInt("boss-health");
        pigZombieDamageMult = getConfig().getDouble("pig-zombie-damage-multiplier");
        tankZombieDamageMult = getConfig().getDouble("tank-zombie-damage-multiplier");
        tankZombieReductionMult = getConfig().getDouble("tank-zombie-damage-reduction-multiplier");
        pigZombieSpawn = getConfig().getInt("pig-zombie-min-wave-spawn");
        tankZombieSpawn = getConfig().getInt("tank-zombie-min-wave-spawn");
        babyZombieWave = getConfig().getInt("babyzombie-wave-increment");
        wolfWave = getConfig().getInt("wolf-wave-increment");
        speedyZombieSpawn = getConfig().getInt("speedy-zombie-min-wave-spawn");
        damageMulitplier = getConfig().getInt("damage-multiplier");
        ChatUtil.setPrefix(msg.getString("PREFIX"));

        if (!setupEconomy()) {
            Bukkit.getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        Bukkit.getScheduler().runTaskTimer(this, () -> Bukkit.getPluginManager().callEvent(new TickEvent(tick++)), 1, 1);

        for (String s : getConfig().getStringList("drops")) {
            String[] args = s.split(":");
            if (args.length < 4)
                continue;

            ItemStack stack = new ItemStack(Material.valueOf(args[0]), Integer.parseInt(args[1]), Byte.parseByte(args[2]));
            double chance = Double.parseDouble(args[3]);
            int weight = (int) chance * 10_000;

            drops.add(new ZombieItem(chance, stack), weight);
        }

        fh.loadArenas();
        fh.loadLobbySigns();

        PerkManager.setupPerks();
        new BiteEffectManager();

        registerCommands();
        registerListeners();
        registerTasks();

        for (Player p : Bukkit.getOnlinePlayers())
            new ZombiePlayer(p);

    }

    @Override
    public void onDisable() {
        for (Arena arena : Arena.arenas) {
            ArenaManager.endArena(arena);
        }
        try {
            fh.saveArenas();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fh.saveLobbySigns();
    }

    public void registerTasks() {
        new MobTask().runTaskTimer(this, 20L, 20L);
        new MobTarget().runTaskTimer(this, 5L, 5L);
        new SignTask().runTaskTimer(this, 20L, 20L);
        new ScoreboardTask().runTaskTimer(this, 40L, 40L);
        new CompassTask().runTaskTimer(this, 20L, 20L);
        Bukkit.getScheduler().runTaskTimer(this, () -> Bukkit.getWorlds().get(0).setTime(14000), 100L, 100L);
    }

    public void registerCommands() {
        CommandManager cm = new CommandManager(this);

        PluginCommand zs = getCommand("zs");
        zs.setExecutor(cm);
        zs.setTabCompleter(cm);

        PluginCommand join = getCommand("join");
        join.setExecutor(cm);
        join.setTabCompleter(cm);
    }

    public void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new BlockBreak(), this);
        Bukkit.getPluginManager().registerEvents(new EntityDeath(), this);
        Bukkit.getPluginManager().registerEvents(new Interact(), this);
        Bukkit.getPluginManager().registerEvents(new Join(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);
        Bukkit.getPluginManager().registerEvents(new Quit(), this);
        Bukkit.getPluginManager().registerEvents(new Tick(), this);
        Bukkit.getPluginManager().registerEvents(new EntityDamage(), this);
        Bukkit.getPluginManager().registerEvents(new Command(), this);
        Bukkit.getPluginManager().registerEvents(new EntityShootBow(), this);
        Bukkit.getPluginManager().registerEvents(new PerkListener(), this);
    }

    public static Main getInstance() {
        return instance;
    }

    public static WorldEdit getWorldEdit() {
        return WorldEdit.getInstance();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static FileHandler getFileHandler() {
        return fh;
    }

    public static ZombieMessage getMsg() {
        return msg;
    }
}
