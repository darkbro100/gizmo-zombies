package me.mario.zs.perks;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;

public class NukePerk extends Perk {

	@Override
	public void run(Player player, Arena arena) {
		ZombiePlayer zp = ZombiePlayer.get(player);

		if (zp.getArena() == null)
			return;
		
//		ArrayList<Entity> toRemove = zp.getArena().getZombiesSpawned();
		if(zp.getArena() != null) {
			new ArrayList<>(zp.getArena().getZombiesSpawned()).stream().forEach(e -> {
				((LivingEntity)e).setFireTicks(10);
				((LivingEntity)e).damage(((LivingEntity)e).getMaxHealth());
				e.getLocation().getWorld().playEffect(e.getLocation(), Effect.SMOKE, 1);
				if(zp.getArena() != null) zp.getArena().getZombiesSpawned().remove(e);
			});
		}
//		for(Entity ent : zp.getArena().getZombiesSpawned()) {
//			((LivingEntity)ent).damage(Integer.MAX_VALUE);
//			ent.getLocation().getWorld().playEffect(ent.getLocation(), Effect.EXPLOSION, 1);
//		}
//		zp.getArena().getZombiesSpawned().removeAll(toRemove);
	}

	@Override
	public int chance() {
		return 1;
	}

	@Override
	public void item(Location toSpawn) {
		Item item = toSpawn.getWorld().dropItemNaturally(toSpawn, new ItemStack(Material.TNT));
		item.setMetadata("NUKE", new FixedMetadataValue(Main.getInstance(), true));
		
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				item.remove();
			}
		}, 20L * 20L);
	}

}
