package me.mario.zs.perks;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;

import me.mario.zs.arena.Arena;

public class PerkManager {
	
	protected PerkManager () { }
	
	public static void setupPerks() {
		Perk.perks.add(new NukePerk());
		Perk.perks.add(new DoublePoints());
		Perk.perks.add(new Godmode());
		Perk.perks.add(new Incinerate());
		Perk.perks.add(new InstaKill());
	}
	
	public static boolean dropPerk(Arena arena, Location where) {
		if(arena == null) return false;
		if(where == null) return false;
		if(arena.getCurrentWave() >= arena.getMaxWaves()) return false;
		ArrayList<Perk> perks = new ArrayList<Perk>();
		for(Perk perk : Perk.perks) {
			for(int i = 0; i < perk.chance(); i++) {
				perks.add(perk);
			}
		}
		if(perks.size() < 300) {
			for(int i = 0; i < (300 - perks.size()); i++) {
				perks.add(null);
			}
		}
		
		Perk toDrop = perks.get(new Random().nextInt(perks.size()));
		if(toDrop == null) return false;
		
		toDrop.item(where);
		return true;
	}
}