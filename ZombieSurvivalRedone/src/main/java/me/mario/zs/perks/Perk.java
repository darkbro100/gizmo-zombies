package me.mario.zs.perks;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.mario.zs.arena.Arena;

public abstract class Perk {

	public static ArrayList<Perk> perks = new ArrayList<Perk>();
	
	public abstract void item(Location toSpawn);
	public abstract void run(Player player, Arena arena);
	public abstract int chance();
	
}
