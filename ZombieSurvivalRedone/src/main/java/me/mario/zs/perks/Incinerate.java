package me.mario.zs.perks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;

public class Incinerate extends Perk {

	private Incinerate instance;
	
	@Override
	public void run(Player player, Arena arena) {
		instance = this;
		ZombiePlayer zp = ZombiePlayer.get(player);

		if (zp.getArena() == null)
			return;
		
		if(arena.getActiveParks().contains(instance))
			return;
		
		arena.getActiveParks().add(instance);
		
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				arena.getActiveParks().remove(instance);
				arena.sendMessage(Main.getMsg().getString("ARENA_PERK.INCINERATE_OVER"));
			}
		}, 20L * 30L);
	}

	@Override
	public int chance() {
		return 1;
	}

	@Override
	public void item(Location toSpawn) {
		Item item = toSpawn.getWorld().dropItemNaturally(toSpawn, new ItemStack(Material.FIRE_CHARGE));
		item.setMetadata("INCINERATE", new FixedMetadataValue(Main.getInstance(), true));
		
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				item.remove();
			}
		}, 20L * 20L);
	}

}
