package me.mario.zs;

import org.bukkit.inventory.ItemStack;

public class ZombieItem {

	private ItemStack item;
	private double chance;
	
	public ZombieItem(double chance, ItemStack item) {
		this.setItem(item);
		this.setChance(chance);
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}

	public double getChance() {
		return chance;
	}

	public void setChance(double chance) {
		this.chance = chance;
	}
	
}
