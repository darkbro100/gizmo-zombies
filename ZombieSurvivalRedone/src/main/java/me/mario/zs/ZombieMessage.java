package me.mario.zs;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class ZombieMessage {
	
	private Main plugin;
	private File file;
    private YamlConfiguration configuration;

    public ZombieMessage(Main plugin) {
    	this.setPlugin(plugin);
    	
    	plugin.saveResource("lang.yml", false);
        file = new File(plugin.getDataFolder(), "lang.yml");
        configuration = YamlConfiguration.loadConfiguration(file);
    }
	
	public String getString(String path) {
		if (configuration.contains(path))
			return ChatColor.translateAlternateColorCodes('&', configuration.getString(path));
		return null;
	}

	public Main getPlugin() {
		return plugin;
	}

	public void setPlugin(Main plugin) {
		this.plugin = plugin;
	}
}