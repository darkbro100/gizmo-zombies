package me.mario.zs.tasks;

import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;

import me.mario.zs.ZombiePlayer;

public class CompassTask extends BukkitRunnable {

	public void run() {
		for(ZombiePlayer zp : ZombiePlayer.zPlayers) {
			if(zp.getArena() == null ) continue;
			
			Entity closest = null;
			double closestDis = 99999999;
			for(Entity ent : zp.getPlayer().getNearbyEntities(128, 128, 128)) {
				if(ent instanceof Zombie || ent instanceof Piglin || ent instanceof Giant || ent instanceof Wolf) {
					if(ent.getLocation().distance(zp.getPlayer().getLocation()) <= closestDis) {
						closest = ent;
						closestDis = ent.getLocation().distance(zp.getPlayer().getLocation());
					}
				}
			}
			
			if(closest != null) {
				zp.getPlayer().setCompassTarget(closest.getLocation());
			}
		}
	}
	
}
