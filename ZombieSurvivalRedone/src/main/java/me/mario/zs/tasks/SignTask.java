package me.mario.zs.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import me.mario.zs.signs.ReviveSign;

public class SignTask extends BukkitRunnable {

	public void run() {
		for(ReviveSign sign : ReviveSign.signs) {
			if(sign.getTimeRemaining() <= 0) {
				sign.getLocation().getBlock().setType(sign.getBlockBefore());
				ReviveSign.signs.remove(sign);
				break;
			}
			
			sign.setLines();
			sign.setTimeRemaining(sign.getTimeRemaining() - 1);
		}
	}
	
}
