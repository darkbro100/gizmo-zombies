package me.mario.zs.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;

public class MobTask extends BukkitRunnable {

	@Override
	public void run() {
		for (Arena arena : Arena.arenas) {
			if(arena.isStarted() && arena.getMobsSpawned() <= arena.getMaxMobs()) {
				if(arena.getCurrentWave() >= arena.getMaxWaves()) {
					ArenaManager.spawnMobs(1, arena);
				} else {
					ArenaManager.spawnMobs(2, arena);
				}
			}
		}
	}
	
}
