package me.mario.zs.tasks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.mario.zs.util.ScoreboardUtil;

public class ScoreboardTask extends BukkitRunnable {

	@Override
	public void run() {
		for(Player player : Bukkit.getOnlinePlayers()) {
			player.setScoreboard(ScoreboardUtil.scoreboard(player));
		}
	}
}
