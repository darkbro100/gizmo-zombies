package me.mario.zs.tasks;

import java.util.ArrayList;

import net.minecraft.world.entity.EntityLiving;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPigZombie;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftWolf;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.scheduler.BukkitRunnable;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;

public class MobTarget extends BukkitRunnable {

	@Override
	public void run() {
		for(Arena arena : Arena.arenas) {
			if(arena.isStarted()) {
				ArrayList<Entity> toRemove = new ArrayList<Entity>();
				for(Entity ent : new ArrayList<>(arena.getZombiesSpawned())) {
					if(ent instanceof Wolf && getNearest(ent) != null) {
						Wolf wolf = (Wolf) ent;
						wolf.setTarget(((LivingEntity) getNearest(wolf).getBukkitEntity()));
					}
					
					if(ent instanceof PigZombie && getNearest(ent) != null) {
						((CraftPigZombie)ent).getHandle().setGoalTarget(getNearest(ent), TargetReason.CLOSEST_PLAYER, false);
					}
					
					if(ent.isDead() || ent == null || !ent.isValid()) {
						toRemove.add(ent);
					}
				}
				arena.getZombiesSpawned().removeAll(toRemove);
				arena.setMobsSpawned(arena.getMobsSpawned() - toRemove.size());
			}
		}
	}
	
	private EntityLiving getNearest(Entity ent) {
		for(Entity near : ent.getNearbyEntities(16L, 16L, 16L)) {
			if(near instanceof Player) {
				Player player = (Player) near;
				ZombiePlayer zp = ZombiePlayer.get(player);
				if(zp.isDead() || zp.getArena() == null) continue;
				
				return ((CraftPlayer)player).getHandle();
			}
		}
		return null;
	}
}
