package me.mario.zs.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.perks.Perk;

public class ScoreboardUtil {

	public static Scoreboard scoreboard(Player player) {
		Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		
		ZombiePlayer zp = ZombiePlayer.get(player);
		
		Objective o = sb.registerNewObjective("stats", "dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		o.setDisplayName(ChatColor.BLUE + ChatColor.BOLD.toString() + "Liquid" + ChatColor.DARK_RED + ChatColor.BOLD + "Z");
		
		Objective objective = sb.registerNewObjective("showhealth", "health");
		objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
		objective.setDisplayName(ChatColor.RED + "Health");
		
		if(player.getHealth() - 0.000001D > 0) {
			player.setHealth(player.getHealth() - 0.000001D);
		}
		
		if(zp.getArena() == null) {
			Score available = o.getScore("\247aNOT IN MATCH");
			available.setScore(11);
			
			Score blank1 = o.getScore("\247a");
			blank1.setScore(10);
			
			String balanceStr = "Bal: \247e" + me.mario.zs.Main.econ.getBalance(player);
			Score balance = o.getScore(balanceStr.length() > 16 ? balanceStr.substring(0, 16) : balanceStr);
			balance.setScore(9);
		} else {
			String availableStr = "\247aMAP: " + zp.getArena().getName();
			Score available = o.getScore(availableStr.length() > 16 ? availableStr.substring(0, 16) : availableStr);
			available.setScore(11);
			
			Score blank1 = o.getScore("\247a");
			blank1.setScore(10);
			
			String balanceStr = "Balance: \247e" + me.mario.zs.Main.econ.getBalance(player);
			Score balance = o.getScore(balanceStr.length() > 16 ? balanceStr.substring(0, 16) : balanceStr);
			balance.setScore(9);
			
			Score blank2 = o.getScore("\247b");
			blank2.setScore(8);
			
			Score wave = o.getScore("Wave: \247a" + zp.getArena().getCurrentWave());
			wave.setScore(7);
			
			Score mobs = o.getScore("Mobs Spawned: \247c" + zp.getArena().getZombiesSpawned().size());
			mobs.setScore(6);
			
			if(zp.getArena().getActiveParks().size() > 0) {
				Score blank3 = o.getScore("\2478");
				blank3.setScore(5);
				
				Score perks = o.getScore("Active Perks:");
				perks.setScore(4);
				
				int i = 3;
				for(Perk perk : zp.getArena().getActiveParks()) {
					String name = perk.getClass().getName();
					String className = name.split("me.mario.zs.perks.")[1];
					Score namePerk = o.getScore("\2476" + className.replaceAll(".class", ""));
					namePerk.setScore(i);
					i--;
				}
			}
		}
		return sb;
	}
	
}
