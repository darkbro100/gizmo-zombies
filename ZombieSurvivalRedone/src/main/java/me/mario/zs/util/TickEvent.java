package me.mario.zs.util;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TickEvent extends Event {
	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	private long tick;

	public TickEvent(long tick) {
		this.tick = tick;
	}

	public long getTick() {
		return tick;
	}
}