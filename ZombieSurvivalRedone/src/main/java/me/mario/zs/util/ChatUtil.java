package me.mario.zs.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtil {

	private static String prefix;
	
	public static void sendMessage(Player player, String msg) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
	}
	
	public static void broadcast(String msg) {
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
	}
	
	public static void setPrefix(String newPrefix) {
		prefix = newPrefix;
	}
	
}
