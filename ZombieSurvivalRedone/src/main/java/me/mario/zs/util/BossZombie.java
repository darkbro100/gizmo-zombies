package me.mario.zs.util;

import net.minecraft.world.entity.EntityTypes;
import net.minecraft.world.entity.ai.goal.PathfinderGoalFloat;
import net.minecraft.world.entity.ai.goal.PathfinderGoalLookAtPlayer;
import net.minecraft.world.entity.ai.goal.PathfinderGoalMeleeAttack;
import net.minecraft.world.entity.ai.goal.target.PathfinderGoalNearestAttackableTarget;
import net.minecraft.world.entity.monster.EntityGiantZombie;
import net.minecraft.world.entity.player.EntityHuman;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;
import org.bukkit.entity.Giant;

import java.lang.reflect.Field;

public class BossZombie extends EntityGiantZombie {

	public BossZombie(net.minecraft.world.level.World world) {
		super(EntityTypes.G, world);
	}

//	@Override
//	protected void initPathfinder() {
//		this.goalSelector.a(1, new PathfinderGoalFloat(this));
//		this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, 1.0D, false));
//		this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
//		this.targetSelector.a(3, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, true));
//	}

	public static BossZombie spawn(Location loc) {
		org.bukkit.World world = loc.getWorld();
		net.minecraft.world.level.World mcWorld = ((CraftWorld) world).getHandle();
		BossZombie zombie = new BossZombie(mcWorld);

		//		zombie.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		zombie.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		((CraftWorld) world).getHandle().addEntity(zombie);
		zombie.getEntity().setRemoveWhenFarAway(false);
		zombie.getEntity().setSilent(true);

		return zombie;
	}

	public Giant getEntity() {
		return (Giant) getBukkitEntity();
	}

	@Override
	public void setOnFire(int ticks) {

	}

	public static Object getPrivateField(String fieldName, Class clazz, Object object) {
		Field field;
		Object o = null;

		try {
			field = clazz.getDeclaredField(fieldName);

			field.setAccessible(true);

			o = field.get(object);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return o;
	}
}