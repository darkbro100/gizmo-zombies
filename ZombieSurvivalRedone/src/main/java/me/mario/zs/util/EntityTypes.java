package me.mario.zs.util;

import java.util.Map;

import net.minecraft.world.entity.Entity;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;

public enum EntityTypes
{
    //NAME("Entity name", Entity ID, yourcustomclass.class);
    CUSTOM_GIANT("Giant", 53, BossZombie.class); //You can add as many as you want.

    private EntityTypes(String name, int id, Class<? extends Entity> custom)
    {
        addToMaps(custom, name, id);
    }

  public static Entity spawnEntity(Entity entity, Location loc)
   {
     entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
     ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity);
     return entity;
   }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	private static void addToMaps(Class clazz, String name, int id)
    {
        //getPrivateField is the method from above.
        //Remove the lines with // in front of them if you want to override default entities (You'd have to remove the default entity from the map first though).
        ((Map)BossZombie.getPrivateField("c", EntityTypes.class, null)).put(name, clazz);
        ((Map)BossZombie.getPrivateField("d", EntityTypes.class, null)).put(clazz, name);
        ((Map)BossZombie.getPrivateField("f", EntityTypes.class, null)).put(clazz, Integer.valueOf(id));
    }
}
