package me.mario.zs.util;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	private ItemStack stack;
	
	public ItemBuilder(Material material) {
		stack = new ItemStack(material);
	}
	
	public ItemBuilder setAmount(int amount) {
		stack.setAmount(amount);
		return this;
	}
	
	public ItemBuilder setName(String name) {
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		stack.setItemMeta(meta);
		return this;
	}
	
	public ItemBuilder setLore(String... lore) {
		ItemMeta meta = stack.getItemMeta();
		meta.setLore(Arrays.asList(lore));
		stack.setItemMeta(meta);
		return this;
	}
	
	public ItemStack build() {
		return stack;
	}
	
}
