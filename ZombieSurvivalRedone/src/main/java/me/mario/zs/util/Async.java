package me.mario.zs.util;

import lombok.Getter;
import me.mario.zs.Main;

public class Async {

	@Getter
	private TaskBuilder builder;

	public Async() {
		builder = TaskBuilder.buildAsync(Main.getInstance());
	}
	
	public static TaskBuilder get() {
		return new Async().getBuilder();
	}
}
