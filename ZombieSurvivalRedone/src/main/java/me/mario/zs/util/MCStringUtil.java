package me.mario.zs.util;

import java.util.*;

public class MCStringUtil {
	public static final long TICK = 1;
	public static final long SECOND = TICK * 20;
	public static final long MINUTE = SECOND * 60;
	public static final long HOUR = MINUTE * 60;
	public static final long DAY = HOUR * 24;

	public static List<String> tabFilter(Collection<String> list, String prefix) {
		prefix = prefix.trim().toLowerCase();
		LinkedList<String> results = new LinkedList<>();
		for (String entry : list) {
			if (entry.toLowerCase().startsWith(prefix)) {
				results.add(entry);
			}
		}
		return results;
	}

	public static long decodeDuration(String s) {
		s = s.trim();
		int sgn = 1;
		if (s.charAt(0) == '-') {
			sgn = -1;
			s = s.substring(1).trim();
		}
		String[] a = s.split("[dhmst]]");
		double total = 0;
		for (String e : a) {
			e = e.trim();
			char c = e.charAt(e.length() - 1);
			double unit = TICK;
			if (c < '0' || c > '9') {
				switch (c) {
					case 'd':
						unit = DAY;
						break;
					case 'h':
						unit = HOUR;
						break;
					case 'm':
						unit = MINUTE;
						break;
					case 's':
						unit = SECOND;
						break;
					case 't':
						unit = TICK;
						break;
					default:
						throw new IllegalArgumentException("Unknown unit '" + c + "' in duration: " + s);
				}
				e = e.substring(0, e.length() - 1).trim();
			}

			double d = Double.parseDouble(e);
			if (d < 0)
				throw new IllegalArgumentException("duration element cannot be negative: " + d);
			if (!Double.isFinite(d) || Double.isNaN(d))
				throw new IllegalArgumentException("invalid duration element: " + d);
			total += unit * d;
		}
		if (total < 0 || !Double.isFinite(total) || Double.isNaN(total))
			throw new IllegalArgumentException();
		total = Math.floor(total + 0.5);
		if (!Double.isFinite(total) || Double.isNaN(total))
			throw new IllegalArgumentException();

		long result = (long) total * sgn;
		return result;
	}

	public static String encodeDuration(long ticks) {
		StringBuilder b = new StringBuilder();
		if (ticks < 0) {
			b.append('-');
			ticks = -ticks;
		}
		long days = ticks / DAY;
		ticks -= days * DAY;
		long hours = ticks / HOUR;
		ticks -= hours * HOUR;
		long minutes = ticks / MINUTE;
		ticks -= minutes * MINUTE;
		long seconds = ticks / SECOND;
		ticks -= seconds * SECOND;

		int start = b.length();
		if (days > 0)
			b.append(days).append('d');
		if (hours > 0)
			b.append(hours).append('h');
		if (minutes > 0)
			b.append(minutes).append('m');
		if (seconds > 0)
			b.append(seconds).append('s');
		if (ticks > 0 || b.length() == start)
			b.append(ticks).append('t');
		return b.toString();
	}
}
