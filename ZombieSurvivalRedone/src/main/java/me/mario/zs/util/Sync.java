package me.mario.zs.util;

import lombok.Getter;
import me.mario.zs.Main;

public class Sync {

	@Getter
	private TaskBuilder builder;

	public Sync() {
		builder = TaskBuilder.buildSync(Main.getInstance());
	}
	
	public static TaskBuilder get() {
		return new Sync().getBuilder();
	}
}
