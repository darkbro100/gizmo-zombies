package me.mario.zs.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mario.zs.Main;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.signs.LobbySign;

public class FileHandler {
	
	private File lobbySignDirectory;
	private File arenaDirectory;
	
	public FileHandler() {
		try {
			File dir = Main.getInstance().getDataFolder();
			if (!dir.exists())
				dir.mkdir();
			
			lobbySignDirectory = new File(dir.getAbsolutePath() + "/LobbySigns");
			if (!lobbySignDirectory.exists())
				lobbySignDirectory.mkdir();
			
			arenaDirectory = new File(dir.getAbsolutePath() + "/Arenas");
			if (!arenaDirectory.exists())
				arenaDirectory.mkdir();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void set(File fileToSet, String path, Object o) throws IOException {
		FileConfiguration fc = YamlConfiguration.loadConfiguration(fileToSet);
		fc.set(path, o);
		fc.save(fileToSet);
	}
	
	public Object get(File fileToGet, String path) {
		FileConfiguration fc = YamlConfiguration.loadConfiguration(fileToGet);
		return fc.get(path);
	}
	
	public void loadLobbySigns() {
		for (File lobbySignFile : lobbySignDirectory.listFiles()) {
			Location loc = LocUtil.locFromString((String) get(lobbySignFile, "location"));
			Arena arena = ArenaManager.getArena((String) get(lobbySignFile, "arena"));

			new LobbySign(arena, loc);
		}
	}

	public void saveLobbySigns() {
		try {
			int i = 0;
			for (LobbySign sign : LobbySign.signs) {
				File file = new File(lobbySignDirectory, i + ".yml");
				if (!(file.exists()))
					file.createNewFile();

				set(file, "location", LocUtil.locToString(sign.getLocation()));
				set(file, "arena", sign.getArena().getName());
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		LobbySign.signs.clear();
	}
	
	@SuppressWarnings("unchecked")
	public void loadArenas() {
		for(File arenaFile : arenaDirectory.listFiles()) {
			HashMap<String, Object> serialized = (HashMap<String, Object>) loadObject(arenaFile);
			Arena.deserialize(serialized);
		}
		//RIP CODE 2015 - 2015
		
//		for (File arenaFile : arenaDirectory.listFiles()) {
//			String name = (String) get(arenaFile, "name");
//			int maxPlayers = (Integer) get(arenaFile, "maxPlayers");
//			int maxWaves = (Integer) get(arenaFile, "maxWaves");
//			int maxMobs = (Integer) get(arenaFile, "maxMobs");
//			int spawnMultiplier = (Integer) get(arenaFile, "spawnMultiplier");
//			int healthMultiplier = (Integer) get(arenaFile, "healthMultiplier");
//			Location spawnLocation = LocUtil.locFromString((String) get(arenaFile, "spawnLocation"));
//			boolean donatorMap = (Boolean) get(arenaFile, "isDonatorMap");
//			HashMap<Location, Integer> waveSpawns = new HashMap<>();
//			HashMap<ArrayList<Location>, Integer> doorSpawns = new HashMap<>();
//			
//			for (String s : ((String) get(arenaFile, "waveSpawns")).split("/")) {
//				String[] args = s.split(":");
//				Location loc = LocUtil.locFromString(args[0]);
//				int wave = Integer.parseInt(args[1]);
//				waveSpawns.put(loc, wave);
//			}
//			
//			for (String s : ((String) get(arenaFile, "doorSpawns")).split("/")) {
//				String[] args = s.split(":");
//				Location loc1 = LocUtil.locFromString(args[0]);
//				Location loc2 = LocUtil.locFromString(args[1]);
//				int wave = Integer.parseInt(args[2]);
//				
//				doorSpawns.put((ArrayList<Location>) Arrays.asList(loc1, loc2), wave);
//			}
//			
//			Arena arena = new Arena(name);
//			arena.setMaxPlayers(maxPlayers);
//			arena.setMaxWaves(maxWaves);
//			arena.setMaxMobs(maxMobs);
//			arena.setSpawnMulitplier(spawnMultiplier);
//			arena.setHealthMultiplier(healthMultiplier);
//			arena.setSpawnLocation(spawnLocation);
//			arena.setDonatorMap(donatorMap);
//			arena.setWaveSpawns(waveSpawns);
//			arena.setDoorSpawns(doorSpawns);
//		}
	}

	public void saveArenas() throws IOException {
		for(Arena arena : Arena.arenas) {
			File file = new File(arenaDirectory.getAbsolutePath() + "/" + arena.getName() + ".txt");
			if(!file.exists()) file.createNewFile();
			setObject(file, arena.serialize());
		}
		//RIP CODE 2015 - 2015
//		try {
//			int i = 0;
//			for (Arena arena : Arena.arenas) {
//				File file = new File(arenaDirectory, i + ".yml");
//				if (!(file.exists()))
//					file.createNewFile();
//
//				set(file, "name", arena.getName());
//				set(file, "maxPlayers", arena.getMaxPlayers());
//				set(file, "maxWaves", arena.getMaxWaves());
//				set(file, "maxMobs", arena.getMaxMobs());
//				set(file, "spawnMultiplier", arena.getSpawnMulitplier());
//				set(file, "healthMultiplier", arena.getHealthMultiplier());
//				set(file, "spawnLocation", LocUtil.locToString(arena.getSpawnLocation()));
//				set(file, "isDonatorMap", arena.isDonatorMap());
//				String waveSpawns = "";
//				String doorSpawns = "";
//				boolean first = true;
//				
//				for (Location loc : arena.getWaveSpawns().keySet()) {
//					if (first) {
//						first = false;
//						waveSpawns = LocUtil.locToString(loc) + ":" + arena.getWaveSpawns().get(loc);
//						continue;
//					}
//					waveSpawns = waveSpawns + "/" + LocUtil.locToString(loc) + ":" + arena.getWaveSpawns().get(loc);
//				}
//				
//				first = true;
//				boolean firstLoc = true;
//				
//				for (ArrayList<Location> locs : arena.getDoorSpawns().keySet()) {
//					if (first) {
//						first = false;
//						for (Location loc : locs) {
//							if (firstLoc) {
//								firstLoc = false;
//								doorSpawns = doorSpawns + LocUtil.locToString(loc);
//								continue;
//							}
//							doorSpawns = doorSpawns + ":" + LocUtil.locToString(loc);
//						}
//						doorSpawns = doorSpawns + ":" + arena.getDoorSpawns().get(locs);
//						continue;
//					}
//					
//					firstLoc = true;
//					
//					for (Location loc : locs) {
//						if (firstLoc) {
//							firstLoc = false;
//							doorSpawns = doorSpawns + "/" + LocUtil.locToString(loc);
//							continue;
//						}
//						doorSpawns = doorSpawns + ":" + LocUtil.locToString(loc);
//					}
//					doorSpawns = doorSpawns + ":" + arena.getDoorSpawns().get(locs);
//				}
//				
//				set(file, "waveSpawns", waveSpawns);
//				set(file, "doorSpawns", doorSpawns);
//				
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		Arena.arenas.clear();
	}
	
	public void setObject(File toSetTo, Object o) {
		try {
			System.out.println("Saving: " + o + " to: " + toSetTo.getAbsolutePath());
			ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(toSetTo));
			output.writeObject(o);
			output.flush();
			output.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Object loadObject(File toLoadFrom) {
		ObjectInputStream input;
		try {
			System.out.println("Loading: " + toLoadFrom.getAbsolutePath());
			input = new ObjectInputStream(new FileInputStream(toLoadFrom));
			Object toReturn = input.readObject();
			input.close();
			return toReturn;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void deleteArenaFile(Arena arena) {
		File file = new File(arenaDirectory.getAbsolutePath() + "/" + arena.getName() + ".txt");
		file.delete();
	}
	
}
