package me.mario.zs.signs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;

public abstract class CustomSign {

	private Location loc;
	private Material blockBefore;
	
	public CustomSign(Location loc) {
		this.loc = loc;
		this.setBlockBefore(this.loc.getBlock().getType());
		if (!(loc.getBlock().getState() instanceof Sign)) {
			loc.getBlock().setType(Material.OAK_SIGN);
		}
	}
	
	public Location getLocation() {
		return loc;
	}
	
	public Sign getSign() {
		return (Sign) loc.getBlock().getState();
	}
	
	public abstract void setLines();

	public Material getBlockBefore() {
		return blockBefore;
	}

	public void setBlockBefore(Material blockBefore) {
		this.blockBefore = blockBefore;
	}
	
}
