package me.mario.zs.signs;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.block.Sign;

import me.mario.zs.Main;
import me.mario.zs.arena.Arena;

public class LobbySign extends CustomSign {
	
	public static ArrayList<LobbySign> signs = new ArrayList<>();
	
	private Arena arena;

	public LobbySign(Arena arena, Location loc) {
		super(loc);
		this.arena = arena;
		
		signs.add(this);
	}
	
	public Arena getArena() {
		return arena;
	}
	
	public void setArena(Arena arena) {
		this.arena = arena;
	}
	
	public static LobbySign getSign(Location loc) {
		return signs.stream().filter(s -> s.getLocation().equals(loc)).findAny().orElse(null);
	}

	@Override
	public void setLines() {
		Sign sign = (Sign) getLocation().getBlock().getState();
		sign.setLine(0, Main.getMsg().getString("ARENA_SIGN_LINES.LINE_1").replace("%arena%", arena.getName())
				.replace("%wave%", arena.getCurrentWave() + "").replace("%size%", arena.getPlayersInGame().size() + "")
				.replace("%max_size%", arena.getMaxPlayers() + ""));

		sign.setLine(1, Main.getMsg().getString("ARENA_SIGN_LINES.LINE_2").replace("%arena%", arena.getName())
				.replace("%wave%", arena.getCurrentWave() + "").replace("%size%", arena.getPlayersInGame().size() + "")
				.replace("%max_size%", arena.getMaxPlayers() + ""));

		sign.setLine(2,
				(!arena.isWaiting())
						? Main.getMsg().getString("ARENA_SIGN_LINES.LINE_3.STARTED").replace("%arena%", arena.getName())
								.replace("%wave%", arena.getCurrentWave() + "")
								.replace("%size%", arena.getPlayersInGame().size() + "")
								.replace("%max_size%", arena.getMaxPlayers() + "")
						: Main.getMsg().getString("ARENA_SIGN_LINES.LINE_3.WAITING").replace("%arena%", arena.getName())
								.replace("%wave%", arena.getCurrentWave() + "")
								.replace("%size%", arena.getPlayersInGame().size() + "")
								.replace("%max_size%", arena.getMaxPlayers() + ""));

		sign.setLine(3, Main.getMsg().getString("ARENA_SIGN_LINES.LINE_4").replace("%arena%", arena.getName())
				.replace("%wave%", arena.getCurrentWave() + "").replace("%size%", arena.getPlayersInGame().size() + "")
				.replace("%max_size%", arena.getMaxPlayers() + ""));
		sign.update();
	}
	
}
