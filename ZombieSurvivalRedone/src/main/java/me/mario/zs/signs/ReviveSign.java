package me.mario.zs.signs;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.block.Sign;

import me.mario.zs.Main;
import me.mario.zs.util.LocUtil;

public class ReviveSign extends CustomSign {
	
	public static ArrayList<ReviveSign> signs = new ArrayList<>();
	
	private String deadPlayer;
	private int timeRemaining = 30;

	public ReviveSign(String player, Location loc) {
		super(loc);
		this.deadPlayer = player;
		
		signs.add(this);
	}
	
	@Override
	public void setLines() {
		Sign sign = (Sign) getLocation().getBlock().getState();
		sign.setLine(0, Main.getMsg().getString("REVIVE_SIGN_LINES.LINE_1").replace("%player%", deadPlayer)
				.replace("%time%", timeRemaining + ""));
		sign.setLine(1, Main.getMsg().getString("REVIVE_SIGN_LINES.LINE_2").replace("%player%", deadPlayer)
				.replace("%time%", timeRemaining + ""));
		sign.setLine(2, Main.getMsg().getString("REVIVE_SIGN_LINES.LINE_3").replace("%player%", deadPlayer)
				.replace("%time%", timeRemaining + ""));
		sign.setLine(3, Main.getMsg().getString("REVIVE_SIGN_LINES.LINE_4").replace("%player%", deadPlayer)
				.replace("%time%", timeRemaining + ""));
		sign.update();
	}
	
	public static ReviveSign getSign(Location loc) {
		return signs.stream().filter(s -> LocUtil.matches(s.getLocation(), loc)).findAny().orElse(null);
	}
	
	public static ReviveSign getSign(String name) {
		return signs.stream().filter(s -> s.getDeadPlayer().equalsIgnoreCase(name)).findAny().orElse(null);
	}
	
	public String getDeadPlayer() {
		return deadPlayer;
	}

	public void setDeadPlayer(String deadPlayer) {
		this.deadPlayer = deadPlayer;
	}

	public int getTimeRemaining() {
		return timeRemaining;
	}

	public void setTimeRemaining(int timeRemaining) {
		this.timeRemaining = timeRemaining;
	}
}
