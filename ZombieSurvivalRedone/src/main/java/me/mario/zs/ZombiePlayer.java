package me.mario.zs;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import me.mario.zs.arena.Arena;
import me.mario.zs.signs.ReviveSign;

public class ZombiePlayer {

	public static ArrayList<ZombiePlayer> zPlayers = new ArrayList<ZombiePlayer>();
	
	private boolean dead;
	private Arena arena;
	private Arena buildingArena;
	private Player player;
	private boolean creatingArena = false;
	
	public ZombiePlayer(Player player) {
		this.setPlayer(player);
		this.dead = false;
		zPlayers.add(this);
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
		
		if(!dead && ReviveSign.getSign(getPlayer().getName()) != null) {
			ReviveSign sign = ReviveSign.getSign(player.getName());
			ReviveSign.signs.remove(sign);
			sign.getLocation().getBlock().setType(sign.getBlockBefore());
			getPlayer().setGameMode(GameMode.SURVIVAL);
		}
	}

	public Arena getArena() {
		return arena;
	}

	public void setArena(Arena arena) {
		this.arena = arena;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public static ZombiePlayer get(Player player) {
		for(ZombiePlayer zp : zPlayers) {
			if(zp.getPlayer().getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())) {
				return zp;
			}
		}
		return null;
	}

	public boolean isCreatingArena() {
		return creatingArena;
	}

	public void setCreatingArena(boolean creatingArena) {
		this.creatingArena = creatingArena;
	}

	public Arena getBuildingArena() {
		return buildingArena;
	}

	public void setBuildingArena(Arena buildingArena) {
		this.buildingArena = buildingArena;
	}
	
}
