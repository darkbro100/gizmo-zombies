package me.mario.zs.biteeffects;

import org.bukkit.entity.Player;

public abstract class BiteEffect {

	public abstract void run(Player player);
	
}
