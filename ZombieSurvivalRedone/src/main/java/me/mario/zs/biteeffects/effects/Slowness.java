package me.mario.zs.biteeffects.effects;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.mario.zs.biteeffects.BiteEffect;

public class Slowness extends BiteEffect {

	@Override
	public void run(Player player) {
		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 30, 1));
	}
	
}
