package me.mario.zs.biteeffects;

import java.util.ArrayList;

import me.mario.zs.biteeffects.effects.Fatigue;
import me.mario.zs.biteeffects.effects.Hunger;
import me.mario.zs.biteeffects.effects.Nausea;
import me.mario.zs.biteeffects.effects.Poison;
import me.mario.zs.biteeffects.effects.Slowness;
import me.mario.zs.biteeffects.effects.Weakness;

public class BiteEffectManager {

	public static ArrayList<BiteEffect> effects = new ArrayList<BiteEffect>();
	
	public BiteEffectManager() {
		effects.add(new Hunger());
		effects.add(new Nausea());
		effects.add(new Poison());
		effects.add(new Slowness());
		effects.add(new Weakness());
//		effects.add(new Fatigue());
	}
	
}
