package me.mario.zs.arena;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.signs.ReviveSign;
import me.mario.zs.util.BossZombie;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.Sync;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.world.entity.EntityLiving;
import net.minecraft.world.entity.EnumItemSlot;
import net.minecraft.world.entity.ai.attributes.GenericAttributes;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftWolf;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftZombie;
import org.bukkit.craftbukkit.v1_17_R1.inventory.CraftItemStack;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.UUID;

public class ArenaManager {

    public static Arena getArena(String name) {
        for (Arena arena : Arena.arenas) {
            if (arena.getName().equalsIgnoreCase(name))
                return arena;
        }
        return null;
    }

    public static Arena getArena(Player player) {
        for (Arena arena : Arena.arenas) {
            for (String uuid : arena.getPlayersInGame()) {
                if (uuid.equalsIgnoreCase(player.getUniqueId().toString()))
                    return arena;
            }
        }
        return null;
    }

    private static Entity spawnMob(Arena arena, Location spawnLoc, EntityType type, boolean babyZombie, boolean op,
                                   boolean speedy) {
        Entity ent = spawnLoc.getWorld().spawnEntity(spawnLoc, type); // TODO:
        // spawn
        // custom
        // zombies
        LivingEntity lent = (LivingEntity) ent;
        lent.setMaxHealth((arena.getCurrentWave() * arena.getHealthMultiplier()) + 3);
        lent.setHealth(lent.getMaxHealth());
        lent.setCustomNameVisible(true);
        lent.setCustomName(ChatColor.RED + "" + lent.getHealth() + " HP");

        if (op) {
            ((CraftLivingEntity) lent).getHandle().setSlot(EnumItemSlot.b,
                    CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_CHESTPLATE)));
        } else if (speedy) {
            if (lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1)))
                ;
            ((CraftLivingEntity) lent).getHandle().setSlot(EnumItemSlot.c,
                    CraftItemStack.asNMSCopy(new ItemStack(Material.LEATHER_BOOTS)));
        } else {
            if (lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, (lent.getType() == EntityType.WOLF || lent.getType() == EntityType.PIGLIN) ? 0 : 1)))
                ;
        }

        if (ent instanceof Zombie && babyZombie) {
            Zombie zomb = (Zombie) ent;
            CraftZombie czombie = (CraftZombie) zomb;
            czombie.setBaby(true);

            // I don't know why these little fuckers are still getting speed.
            Sync.get().delay(2).run(() -> {
                czombie.getActivePotionEffects().clear();
            });
        } else if (ent instanceof Zombie && !babyZombie) {
            Zombie zomb = (Zombie) ent;
            CraftZombie czombie = (CraftZombie) zomb;
            czombie.setBaby(false);
        } else if (ent instanceof Wolf) {
            Wolf wolf = (Wolf) ent;
            CraftWolf dog = (CraftWolf) wolf;
            dog.setAngry(true);

            // TRICK DOGS INTO THINKING PLAYER HIT THEM SHIT TIER MINECRAFT.
            Sync.get().delay(5).run(() -> {
                dog.damage(0, getNearest(wolf).getBukkitEntity());
            });
        }

        ((CraftLivingEntity) lent).getHandle().getAttributeInstance(GenericAttributes.b).setValue(100);

        arena.getZombiesSpawned().add(lent);
        arena.setMobsSpawned(arena.getMobsSpawned() + 1);
//		System.out.println("Successfully spawned: " + ent.getType().name() + " at loc: " + LocUtil.locToString(spawnLoc));
        return ent;
    }

    public static void spawnMobs(int amount, Arena arena) {
        for (int i = 0; i < amount; i++) {
            spawnMob(arena);
        }
    }

    private static EntityLiving getNearest(Entity ent) {
        for (Entity near : ent.getNearbyEntities(16L, 16L, 16L)) {
            if (near instanceof Player) {
                Player player = (Player) near;
                ZombiePlayer zp = ZombiePlayer.get(player);
                if (zp.isDead() || zp.getArena() == null) continue;

                return ((CraftPlayer) player).getHandle();
            }
        }
        return null;
    }

    public static Entity respawnMob(Arena arena, Location spawnLoc, EntityType type, boolean babyZombie, boolean op,
                                    boolean speedy) {
        Entity ent = spawnLoc.getWorld().spawnEntity(spawnLoc, type); // TODO:
        // spawn
        // custom
        // zombies
        LivingEntity lent = (LivingEntity) ent;
        lent.setMaxHealth((arena.getCurrentWave() * arena.getHealthMultiplier()) + 3);
        lent.setHealth(lent.getMaxHealth());
        lent.setCustomNameVisible(true);
        lent.setCustomName(ChatColor.RED + "" + lent.getHealth() + " HP");

        if (op) {
            ((CraftLivingEntity) lent).getHandle().setSlot(EnumItemSlot.b,
                    CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_CHESTPLATE)));
        } else if (speedy) {
            if (lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1)))
                ;
            ((CraftLivingEntity) lent).getHandle().setSlot(EnumItemSlot.c,
                    CraftItemStack.asNMSCopy(new ItemStack(Material.LEATHER_BOOTS)));
        } else {
            if (lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, (lent.getType() == EntityType.WOLF || lent.getType() == EntityType.PIGLIN) ? 0 : 1)))
                ;
        }

        if (ent instanceof Zombie && babyZombie) {
            Zombie zomb = (Zombie) ent;
            CraftZombie czombie = (CraftZombie) zomb;
            czombie.setBaby(true);
        } else if (ent instanceof Zombie && !babyZombie) {
            Zombie zomb = (Zombie) ent;
            CraftZombie czombie = (CraftZombie) zomb;
            czombie.setBaby(false);
        }

        ((CraftLivingEntity) lent).getHandle().getAttributeInstance(GenericAttributes.b).setValue(100);

//		System.out.println("REspawned: " + ent.getType().name() + " at loc: " + LocUtil.locToString(spawnLoc));
        return ent;
    }

    private static void spawnMob(Arena arena) {
        ArrayList<Location> keys = new ArrayList<>(arena.getWaveSpawns().keySet());
        Collections.shuffle(keys);

        while (arena.getCurrentWave() < arena.getWaveSpawns().get(keys.get(0))) {
            Collections.shuffle(keys);
        }

        Location spawnLoc = keys.get(0);

        if (arena.getCurrentWave() >= arena.getMaxWaves() && arena.getMobsSpawned() < arena.getMaxMobs()) {
            Entity ent = BossZombie.spawn(arena.getSpawnLocation()).getBukkitEntity();
//			System.out.println(ent.getEntityId());
            arena.setMobsSpawned(arena.getMobsSpawned() + 1);
            arena.getZombiesSpawned().add(ent);

        } else if (arena.getCurrentWave() % Main.wolfWave == 0) {
            spawnMob(arena, spawnLoc, (((new Random().nextInt(10) + 1) == 1) && arena.getCurrentWave() >= Main.pigZombieSpawn)
                    ? EntityType.PIGLIN : EntityType.WOLF, false, false, false);

        } else if (arena.getCurrentWave() % Main.babyZombieWave == 0) {
            spawnMob(arena, spawnLoc,
                    (((new Random().nextInt(10) + 1) == 1) && arena.getCurrentWave() >= Main.pigZombieSpawn) ? EntityType.PIGLIN
                            : EntityType.ZOMBIE,
                    true, (arena.getCurrentWave() >= Main.tankZombieSpawn && (new Random().nextInt(4) + 1) == 1),
                    false);

        } else {
            spawnMob(arena, spawnLoc,
                    (((new Random().nextInt(10) + 1) == 1) && arena.getCurrentWave() >= Main.pigZombieSpawn) ? EntityType.PIGLIN
                            : EntityType.ZOMBIE,
                    false, (arena.getCurrentWave() >= Main.tankZombieSpawn && (new Random().nextInt(4) + 1) == 1),
                    (arena.getCurrentWave() >= Main.speedyZombieSpawn && (new Random().nextInt(3) + 1) == 1));
        }
    }

    public static void spawnSpecialMobs(Location where, Arena arena) {
        for (int i = 0; i < 15; i++) {
            Entity ent = spawnMob(arena, where,
                    ((new Random().nextInt(10) + 1) == 1) ? EntityType.PIGLIN : EntityType.ZOMBIE, false,
                    ((new Random().nextInt(4) + 1) == 1), ((new Random().nextInt(3) + 1) == 1));

            double x = (float) -1 + (float) (Math.random() * ((1 - -1) + 1));
            double y = (float) -0.2 + (float) (Math.random() * ((0.1 - -0.1) + 1));
            double z = (float) -1 + (float) (Math.random() * ((1 - -1) + 1));

            ent.setVelocity(new Vector(x, y, z));
        }
    }

    public static void joinArena(Player player, Arena arena) {
        if (arena.getPlayersInGame().size() >= arena.getMaxPlayers()) {
            ChatUtil.sendMessage(player, Main.getMsg().getString("ARENA_MESSAGES.FULL"));
            return;
        }

        if (!arena.isStarted()) {
            ChatUtil.broadcast(Main.getMsg().getString("ARENA_MESSAGES.START").replace("%arena%", arena.getName()));
            arena.setState(ArenaState.STARTED);
            arena.setCurrentWave(1);
            arena.setMobsSpawned(0);
            arena.setDamageMultiplier((Main.damageMulitplier * arena.getCurrentWave()) + 1);
            arena.setMaxMobs(getMaxZombies(arena));
//			System.out.println(String.format("wave: %s, current mobs: %s, max mobs: %s", arena.getCurrentWave(),
//					arena.getMobsSpawned(), arena.getMaxMobs()));
        }

        ChatUtil.sendMessage(player, Main.getMsg().getString("ARENA_MESSAGES.JOINED").replace("%arena%", arena.getName()));
        ChatUtil.broadcast(Main.getMsg().getString("ARENA_MESSAGES.BROADCAST_JOINED").replace("%player%", player.getName()).replace("%arena%", arena.getName()));

        ZombiePlayer.get(player).setArena(arena);
        player.teleport(arena.getSpawnLocation());
        arena.getPlayersInGame().add(player.getUniqueId().toString());
        if (player.getInventory().getItem(9) != null) {
            player.getInventory().addItem(Main.ZOMBIE_FINDER);
        } else {
            player.getInventory().setItem(9, Main.ZOMBIE_FINDER);
        }
    }

    public static void leaveArena(Player player, Arena arena) {
        arena.getPlayersInGame().remove(player.getUniqueId().toString());
        player.teleport(player.getWorld().getSpawnLocation());
        ZombiePlayer.get(player).setArena(null);

        ChatUtil.sendMessage(player, Main.getMsg().getString("ARENA_MESSAGES.LEFT").replace("%arena%", arena.getName()));
        ChatUtil.broadcast(Main.getMsg().getString("ARENA_MESSAGES.BROADCAST_LEFT").replace("%player%", player.getName()).replace("%arena%", arena.getName()));

        if (ReviveSign.getSign(player.getName()) != null) {
            ReviveSign rs = ReviveSign.getSign(player.getName());
            rs.getLocation().getBlock().setType(rs.getBlockBefore());
            ReviveSign.signs.remove(rs);
        }

        if (arena.getPlayersInGame().size() <= 0) {
            endArena(arena);
        } else if (arena.allDead()) {
            endArena(arena);
        }

        player.getInventory().removeItem(Main.ZOMBIE_FINDER);
        player.setGameMode(GameMode.SURVIVAL);
    }

    public static void waveEnd(final Arena arena) {
//		System.out.println("called");
        if (!arena.isStarted())
            return;

        new ArrayList<>(arena.getPlayersInGame()).stream().forEach(player -> {
            ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(player)));
            if (zp.isDead()) {
                zp.getPlayer().teleport(arena.getSpawnLocation());
                zp.setDead(false);
                zp.getPlayer().setGameMode(GameMode.SURVIVAL);

                if (ReviveSign.getSign(zp.getPlayer().getName()) != null) {
                    ReviveSign rs = ReviveSign.getSign(zp.getPlayer().getName());
                    rs.getLocation().getBlock().setType(rs.getBlockBefore());
                    ReviveSign.signs.remove(rs);
                }

                zp.getPlayer().getInventory().addItem(Main.ZOMBIE_FINDER);
            }
            zp.getPlayer().setHealth(zp.getPlayer().getMaxHealth());
            zp.getPlayer().setFoodLevel(20);

            if (arena.getCurrentWave() + 1 > arena.getMaxWaves()) {
                endArena(arena);
            } else {
                arena.setState(ArenaState.INTERMISSION);

                ChatUtil.sendMessage(zp.getPlayer(), Main.getMsg().getString("ARENA_MESSAGES.WAVE_ENDED").replace("%wave%", arena.getCurrentWave() + ""));
                ChatUtil.sendMessage(zp.getPlayer(),
                        Main.getMsg().getString("ARENA_MESSAGES.UPCOMING_WAVE").replace("%wave%", (arena.getCurrentWave() + 1) + ""));
                if (arena.getCurrentWave() + 1 >= arena.getMaxWaves()) {
                    ChatUtil.sendMessage(zp.getPlayer(), Main.getMsg().getString("ARENA_MESSAGES.BOSS_ROUND_INFO"));
                    ChatUtil.sendMessage(zp.getPlayer(), Main.getMsg().getString("ARENA_MESSAGES.BOSS_HEALTH").replace("%health%", Main.bossHealth + ""));
                } else {
                    ChatUtil.sendMessage(zp.getPlayer(), Main.getMsg().getString("ARENA_MESSAGES.MOB_COUNT").replace("%amount%", getMaxZombies(arena) + ""));
                    ChatUtil.sendMessage(zp.getPlayer(),
                            Main.getMsg().getString("ARENA_MESSAGES.MOB_HEALTH").replace("%health%", (((arena.getCurrentWave() + 1) * arena.getHealthMultiplier()) + 3) + ""));
                }
            }
        });

        Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
            public void run() {
                if (arena.getState() == ArenaState.WAITING)
                    return;
                arena.setState(ArenaState.STARTED);
                if ((arena.getCurrentWave() + 1) >= arena.getMaxWaves()) {
                    arena.setMaxMobs(1);
                } else {
                    arena.setMaxMobs(getMaxZombies(arena));
                }
                arena.setCurrentWave(arena.getCurrentWave() + 1);
                arena.setMobsSpawned(0);
                arena.setDamageMultiplier((Main.damageMulitplier * arena.getCurrentWave()) + 1);
//				System.out.println(String.format("wave: %s, current mobs: %s, max mobs: %s", arena.getCurrentWave(),
//						arena.getMobsSpawned(), arena.getMaxMobs()));
                arena.getPlayersInGame().forEach(string -> ChatUtil
                        .sendMessage(Bukkit.getPlayer(UUID.fromString(string)), Main.getMsg().getString("ARENA_MESSAGES.NEW_WAVE").replace("%wave%", arena.getCurrentWave() + "")));
                arena.sendTitle(Main.getMsg().getString("ARENA_TITLES.NEW_WAVE").replace("%wave%", arena.getCurrentWave() + ""), 20, 60, 20);
                if (arena.getCurrentWave() >= arena.getMaxWaves())
                    arena.sendMessage(Main.getMsg().getString("ARENA_MESSAGES.BOSS_ROUND_START"));
                arena.openDoors();
            }
        }, 20L * 20L);
    }

    public static void endArena(Arena arena) {
        ChatUtil.broadcast(Main.getMsg().getString("ARENA_MESSAGES.ARENA_ENDED").replace("%arena%", arena.getName()));
        for (String player : arena.getPlayersInGame()) {
            ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(player)));
            zp.getPlayer().teleport(zp.getPlayer().getWorld().getSpawnLocation());
            zp.setDead(false);
            zp.setArena(null);
            zp.getPlayer().getInventory().removeItem(Main.ZOMBIE_FINDER);
            zp.getPlayer().setGameMode(GameMode.SURVIVAL);
        }

        for (Entity ent : arena.getZombiesSpawned()) {
            ent.remove();
        }

        arena.getZombiesSpawned().clear();
        arena.getPlayersInGame().clear();
        arena.setState(ArenaState.WAITING);
        arena.closeDoors();
    }

    public static Arena getArena(Entity ent) {
        for (Arena arena : Arena.arenas) {
            for (Entity e : arena.getZombiesSpawned()) {
                if (e.getEntityId() == ent.getEntityId())
                    return arena;
            }
        }
        return null;
    }

    public static int getMaxZombies(Arena arena) {
        int max = 0;
        double mz = arena.getSpawnMulitplier() * 100;
        double w = arena.getCurrentWave() + 1;
        if (mz < 10) {
            max = (int) (mz * w * 0.5D);
        }
        if ((mz >= 10) && (mz <= 50)) {
            max = (int) (mz * w * 0.1D);
        }
        if ((mz >= 51) && (mz <= 100)) {
            max = (int) (mz * w * 0.08D);
        }
        if ((mz >= 101) && (mz <= 200)) {
            max = (int) (mz * w * 0.05D);
        }
        if (mz >= 201) {
            max = (int) (mz * w * 0.04D);
        }
        return max;
    }
}
