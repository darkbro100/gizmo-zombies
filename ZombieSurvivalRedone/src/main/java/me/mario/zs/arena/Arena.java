package me.mario.zs.arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.material.Door;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Openable;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.perks.Perk;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.LocUtil;
import me.mario.zs.util.TitleUtil;
import net.md_5.bungee.api.ChatColor;

public class Arena {

	public static final ArrayList<Arena> arenas = new ArrayList<>();

	private Location spawnLocation;
	private HashMap<Location, Integer> waveSpawns = new HashMap<>(); // WAVE,
																						// SPAWN
																						// (-1
																						// means
																						// any
																						// wave)
	private HashMap<ArrayList<Location>, Integer> doorSpawns = new HashMap<>(); // WAVE,
																											// DOOR(-1
																											// means
																											// any
																											// wave)
	private double healthMultiplier;
	private double spawnMulitplier;
	private double damageMultiplier;
	private int maxWaves;
	private int maxPlayers;
	private int currentWave;
	private ArenaState state;
	private String name;
	private boolean donatorMap;
	private boolean bossCooldown;

	private int maxMobs; // PER ROUND
	private int mobsSpawned; // PER ROUND

	private ArrayList<String> playersInGame = new ArrayList<String>();
	private ArrayList<Entity> zombiesSpawned = new ArrayList<Entity>();

	private ArrayList<Perk> activeParks = new ArrayList<Perk>();
	
	public Arena(String name) {
		this.name = name;

		arenas.add(this);
	}

	public Location getSpawnLocation() {
		return spawnLocation;
	}

	public void setSpawnLocation(Location spawnLocation) {
		this.spawnLocation = spawnLocation;
	}

	public HashMap<Location, Integer> getWaveSpawns() {
		return waveSpawns;
	}

	public void setWaveSpawns(HashMap<Location, Integer> waveSpawns) {
		this.waveSpawns = waveSpawns;
	}

	public double getHealthMultiplier() {
		return healthMultiplier;
	}

	public void setHealthMultiplier(double healthMultiplier2) {
		this.healthMultiplier = healthMultiplier2;
	}

	public double getSpawnMulitplier() {
		return spawnMulitplier;
	}

	public void setSpawnMulitplier(double spawnMulitplier) {
		this.spawnMulitplier = spawnMulitplier;
	}

	public int getMaxWaves() {
		return maxWaves;
	}

	public void setMaxWaves(int maxWaves) {
		this.maxWaves = maxWaves;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public int getCurrentWave() {
		return currentWave;
	}

	public void setCurrentWave(int currentWave) {
		this.currentWave = currentWave;
	}

	public ArenaState getState() {
		return state;
	}

	public void setState(ArenaState state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<ArrayList<Location>, Integer> getDoorSpawns() {
		return doorSpawns;
	}

	public void setDoorSpawns(HashMap<ArrayList<Location>, Integer> doorSpawns) {
		this.doorSpawns = doorSpawns;
	}

	public boolean isStarted() {
		return state == ArenaState.STARTED || state == ArenaState.INTERMISSION;
	}
	
	public boolean isWaiting() {
		return state == ArenaState.WAITING;
	}

	public int getMaxMobs() {
		return maxMobs;
	}

	public void setMaxMobs(int maxMobs) {
		this.maxMobs = maxMobs;
	}

	public int getMobsSpawned() {
		return mobsSpawned;
	}

	public void setMobsSpawned(int mobsSpawned) {
		this.mobsSpawned = mobsSpawned;
	}

	public ArrayList<String> getPlayersInGame() {
		return playersInGame;
	}

	public void setPlayersInGame(ArrayList<String> playersInGame) {
		this.playersInGame = playersInGame;
	}

	public ArrayList<Entity> getZombiesSpawned() {
		return zombiesSpawned;
	}

	public void setZombiesSpawned(ArrayList<Entity> zombiesSpawned) {
		this.zombiesSpawned = zombiesSpawned;
	}

	public boolean isDonatorMap() {
		return donatorMap;
	}

	public void setDonatorMap(boolean b) {
		this.donatorMap = b;
	}

	public HashMap<String, Object> serialize() {
		HashMap<String, Object> toReturn = new HashMap<String, Object>();
		toReturn.put("maxWaves", getMaxWaves());
		toReturn.put("maxPlayers", getMaxPlayers());
		toReturn.put("spawnLocation", LocUtil.locToString(getSpawnLocation()));

		HashMap<String, Integer> serializedMap = new HashMap<String, Integer>();
		for (Location loc : getWaveSpawns().keySet()) {
			serializedMap.put(LocUtil.locToString(loc), getWaveSpawns().get(loc));
		}
		toReturn.put("zombieSpawns", serializedMap);

		HashMap<ArrayList<String>, Integer> serializedDoors = new HashMap<ArrayList<String>, Integer>();
		ArrayList<String> serializedList = new ArrayList<String>();
		for (ArrayList<Location> list : getDoorSpawns().keySet()) {
			for (Location loc : list) {
				serializedList.add(LocUtil.locToString(loc));
			}
			serializedDoors.put(serializedList, getDoorSpawns().get(list));
		}
		toReturn.put("doorSpawns", serializedDoors);
		toReturn.put("spawnMultiplier", getSpawnMulitplier());
		toReturn.put("healthMultiplier", getHealthMultiplier());
		toReturn.put("name", getName());

		return toReturn;
	}

	@SuppressWarnings("unchecked")
	public static void deserialize(HashMap<String, Object> map) {
		int maxWaves = (int) map.get("maxWaves");
		int maxPlayers = (int) map.get("maxPlayers");
		Location spawnLocation = LocUtil.locFromString((String) map.get("spawnLocation"));
		HashMap<Location, Integer> zombieSpawns = new HashMap<>();
		HashMap<String, Integer> zombieSpawnsSerialized = (HashMap<String, Integer>) map.get("zombieSpawns");
		for (String s : zombieSpawnsSerialized.keySet()) {
			Location loc = LocUtil.locFromString(s);
			zombieSpawns.put(loc, zombieSpawnsSerialized.get(s));
		}
		HashMap<ArrayList<Location>, Integer> doorSpawns = new HashMap<ArrayList<Location>, Integer>();
		HashMap<ArrayList<String>, Integer> doorSpawnsSerialized = (HashMap<ArrayList<String>, Integer>) map
				.get("doorSpawns");
		ArrayList<Location> deserializedList = new ArrayList<Location>();
		for (ArrayList<String> key : doorSpawnsSerialized.keySet()) {
			for (String str : key) {
				deserializedList.add(LocUtil.locFromString(str));
			}
			doorSpawns.put(deserializedList, doorSpawnsSerialized.get(key));
		}
		double spawnMultiplier = (double) map.get("spawnMultiplier");
		double healthMultiplier = (double) map.get("healthMultiplier");
		String name = (String) map.get("name");

		Arena arena = new Arena(name);
		arena.setHealthMultiplier(healthMultiplier);
		arena.setSpawnLocation(spawnLocation);
		arena.setSpawnMulitplier(spawnMultiplier);
		arena.setHealthMultiplier(healthMultiplier);
		arena.setWaveSpawns(zombieSpawns);
		arena.setDoorSpawns(doorSpawns);
		arena.setMaxPlayers(maxPlayers);
		arena.setMaxWaves(maxWaves);
	}

	public void sendMessage(String string) {
		for (String uuid : playersInGame) {
			ChatUtil.sendMessage(Bukkit.getPlayer(UUID.fromString(uuid)), string);
		}
	}

	public boolean isBossCooldown() {
		return bossCooldown;
	}

	public void setBossCooldown(boolean bossCooldown) {
		this.bossCooldown = bossCooldown;
	}

	public double getDamageMultiplier() {
		return damageMultiplier;
	}

	public void setDamageMultiplier(double damageMultiplier) {
		this.damageMultiplier = damageMultiplier;
	}

	public void sendTitle(String string, int fadein, int fadeout, int stay) {
		for (String str : getPlayersInGame()) {
			ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(str)));
			TitleUtil.sendTitle(zp.getPlayer(), fadein, stay, fadeout, ChatColor.translateAlternateColorCodes('&', string), "");
		}
	}

	public void sendActionBar(String string) {
		for (String str : getPlayersInGame()) {
			ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(str)));
			TitleUtil.sendActionBar(string, zp.getPlayer());
		}
	}

	public void playSound(Sound sound, float pitch, float volume) {
		for (String str : getPlayersInGame()) {
			ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(str)));
			zp.getPlayer().playSound(zp.getPlayer().getLocation(), sound, pitch, volume);
		}
	}

	public void openDoors() {
		boolean success = false;
		for (Entry<ArrayList<Location>, Integer> doors : this.doorSpawns.entrySet()) {
			if (getCurrentWave() == doors.getValue()) {
				Location loc1 = doors.getKey().get(0);
				Location loc2 = doors.getKey().get(1);

				int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
				int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
				int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
				
				int maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
				int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
				int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
				
				for (int x = minX; x <= maxX; x++) {
					for (int y = minY; y <= maxY; y++) {
						for (int z = minZ; z <= maxZ; z++) {
							Location newLoc = new Location(loc1.getWorld(), x, y, z);
							Block b = newLoc.getBlock();
							
							openDoor(b);
							success = true;
						}
					}
				}
			}
		}
		if (success) {
			sendMessage(Main.getMsg().getString("ARENA_MESSAGES.DOORS_OPENED"));
			sendTitle(Main.getMsg().getString("ARENA_TITLES.DOORS_OPENED"), 20, 60, 20);
		}
	}

	public void closeDoors() {
		for (Entry<ArrayList<Location>, Integer> doors : this.doorSpawns.entrySet()) {
			Location loc1 = doors.getKey().get(0);
			Location loc2 = doors.getKey().get(1);
			int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
			int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
			int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
			
			int maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
			int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
			int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
			
			for (int x = minX; x <= maxX; x++) {
				for (int y = minY; y <= maxY; y++) {
					for (int z = minZ; z <= maxZ; z++) {
						Location newLoc = new Location(loc1.getWorld(), x, y, z);
						Block b = newLoc.getBlock();
						closeDoor(b);
					}
				}
			}
		}
	}
	
	private void closeDoor(Block b) {
		setDoorOpen(b, false);
	}
	
	private void openDoor(Block b) {
		setDoorOpen(b, true);
	}
	
	
    public boolean setDoorOpen(Block block, boolean open) {
		org.bukkit.block.data.Openable openable = (org.bukkit.block.data.Openable) block.getBlockData();
		openable.setOpen(open);
		block.setBlockData(openable);
        return true;
    }
	
	public ArrayList<Perk> getActiveParks() {
		return activeParks;
	}

	public void setActiveParks(ArrayList<Perk> activeParks) {
		this.activeParks = activeParks;
	}

	public boolean allDead() {
		boolean allDead = true;
		for(String s : getPlayersInGame()) {
			ZombiePlayer zp = ZombiePlayer.get(Bukkit.getPlayer(s));
			if(!zp.isDead()) allDead = false;
		}
		return allDead;
	}
	
}

enum ArenaState {
	STARTED, WAITING, INTERMISSION;
}
