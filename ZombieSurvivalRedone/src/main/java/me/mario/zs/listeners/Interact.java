package me.mario.zs.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.signs.LobbySign;
import me.mario.zs.util.ChatUtil;

public class Interact implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		
		if(e.getClickedBlock() == null) return;
		
		if (LobbySign.getSign(e.getClickedBlock().getLocation()) == null)
			return;
		LobbySign sign = LobbySign.getSign(e.getClickedBlock().getLocation());
		Arena arena = sign.getArena();
		
		if (arena.isDonatorMap()) {
			ChatUtil.sendMessage(player, Main.getMsg().getString("LOBBY_MESSAGES.DONATOR_ONLY"));
			return;
		}
		
		if (arena.getPlayersInGame().size() >= arena.getMaxPlayers()) {
			ChatUtil.sendMessage(player, Main.getMsg().getString("LOBBY_MESSAGES.ARENA_FULL"));
			return;
		}
		
		if(ZombiePlayer.get(player).getArena() != null) {
			ChatUtil.sendMessage(player, Main.getMsg().getString("LOBBY_MESSAGES.ALREADY_IN"));
			return;
		}
		
		ArenaManager.joinArena(player, arena);
		e.setCancelled(true);
	}
}
