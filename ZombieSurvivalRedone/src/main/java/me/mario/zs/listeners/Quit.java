package me.mario.zs.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.signs.ReviveSign;

public class Quit implements Listener {
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		ZombiePlayer zp = ZombiePlayer.get(e.getPlayer());
		if (zp.getArena() != null)
			ArenaManager.leaveArena(e.getPlayer(), zp.getArena());
		if (ReviveSign.getSign(zp.getPlayer().getName()) != null) {
			ReviveSign.getSign(zp.getPlayer().getName()).getLocation().getBlock().setType(ReviveSign.getSign(zp.getPlayer().getName()).getBlockBefore());
			ReviveSign.signs.remove(ReviveSign.getSign(zp.getPlayer().getName()));
		}
		
		e.getPlayer().teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		ZombiePlayer.zPlayers.remove(zp);
	}
}
