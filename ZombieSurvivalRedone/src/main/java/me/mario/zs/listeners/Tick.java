package me.mario.zs.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import me.mario.zs.signs.LobbySign;
import me.mario.zs.util.TickEvent;

public class Tick implements Listener {
	
	@EventHandler
	public void onTick(TickEvent e) {
		if (e.getTick() % 10 == 0)
			LobbySign.signs.forEach(s -> s.setLines());
	}
}
