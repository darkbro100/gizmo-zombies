package me.mario.zs.listeners;

import java.text.DecimalFormat;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Giant;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.biteeffects.BiteEffectManager;
import me.mario.zs.perks.Godmode;
import me.mario.zs.perks.Incinerate;
import me.mario.zs.perks.InstaKill;
import me.mario.zs.perks.Perk;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.TitleUtil;

public class EntityDamage implements Listener {

	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Zombie || event.getEntity() instanceof Wolf || event.getEntity() instanceof PigZombie || event.getEntity() instanceof Giant) {
			LivingEntity lent = (LivingEntity) event.getEntity();
			lent.setCustomNameVisible(true);
			lent.setCustomName((new DecimalFormat("##.##").format(lent.getHealth())) + " \247cHP");
			
			if(event.getEntity() instanceof Giant) {
				event.setDamage(event.getDamage() / 8);
				Arena arena = ArenaManager.getArena(event.getEntity());
				if(arena.isBossCooldown()) return;
				arena.setBossCooldown(true);
				Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					public void run() {
						arena.setBossCooldown(false);
					}
				}, 20L * 15L);
				ArenaManager.spawnSpecialMobs(event.getEntity().getLocation(), arena);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		boolean godmodeActive = false;
		
		if(event.getEntity() instanceof Player)  {
			Player player = (Player) event.getEntity();
			if(ZombiePlayer.get(player) != null && ZombiePlayer.get(player).getArena() != null) {
				for(Perk perk : ZombiePlayer.get(player).getArena().getActiveParks()) {
					if(perk instanceof Godmode) {
						godmodeActive = true;
						break;
					}
				}
			}
		}
		
		if(event.getDamager() instanceof Giant || event.getDamager() instanceof Zombie || event.getDamager() instanceof Wolf || event.getDamager() instanceof PigZombie) {
			if(ArenaManager.getArena(event.getDamager()) != null) {
				event.setDamage(ArenaManager.getArena(event.getDamager()).getDamageMultiplier());
			}
			
			if(event.getEntity() instanceof Player) {
				if(Math.random() <= 0.05 && !godmodeActive) {
					BiteEffectManager.effects.get(new Random().nextInt(BiteEffectManager.effects.size())).run((Player)event.getEntity());
					ChatUtil.sendMessage((Player)event.getEntity(), Main.getMsg().getString("ARENA_MESSAGES.BITTEN"));
					TitleUtil.sendTitle((Player)event.getEntity(), 20, 60, 20, Main.getMsg().getString("ARENA_TITLES.BITTEN"), "");
				}
			}
		}
		
		if(event.getDamager() instanceof Zombie ||  event.getDamager() instanceof PigZombie && event.getEntity() instanceof Player) {
			Entity damager = event.getDamager();
			
			if(damager.getMetadata("op") != null) {
				event.setDamage(event.getDamage() * Main.tankZombieDamageMult);
			}
		}
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Zombie ||  event.getEntity() instanceof PigZombie || event.getEntity() instanceof Wolf) {
			Entity damaged = event.getEntity();
			
			if(damaged.getMetadata("op") != null) {
				event.setDamage(event.getDamage() * Main.tankZombieReductionMult);
			}

			if(ArenaManager.getArena(damaged) != null) {
				for(Perk perk : ArenaManager.getArena(damaged).getActiveParks()) {
					if(perk instanceof Incinerate) {
						event.getEntity().setFireTicks(200);
					}
					
					if(perk instanceof InstaKill) {
						event.setDamage(Integer.MAX_VALUE);
					}
				}
			}
		}
		
		if(event.getDamager() instanceof PigZombie && event.getEntity() instanceof Player) {
			event.setDamage(event.getDamage() * Main.pigZombieDamageMult);
		}
		
		if(event.getEntity() instanceof Player && godmodeActive) event.setCancelled(true);
	}
	
}
