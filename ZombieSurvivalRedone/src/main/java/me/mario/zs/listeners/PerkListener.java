package me.mario.zs.listeners;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.perks.DoublePoints;
import me.mario.zs.perks.Godmode;
import me.mario.zs.perks.Incinerate;
import me.mario.zs.perks.InstaKill;
import me.mario.zs.perks.NukePerk;
import me.mario.zs.perks.Perk;

public class PerkListener implements Listener {

	@EventHandler
	public void onPlayerPickup(PlayerPickupItemEvent event) {
		Item item = event.getItem();
		Player player = event.getPlayer();
		
		if(item.hasMetadata("NUKE")) {
			ZombiePlayer zp = ZombiePlayer.get(event.getPlayer());
			if(zp.getArena() != null) {
				zp.getArena().sendMessage(Main.getMsg().getString("ARENA_PERK.NUKE"));
				zp.getArena().sendTitle(Main.getMsg().getString("ARENA_ACTIONBAR.NUKE"), 20, 20, 60);
				
				for(Perk perk : Perk.perks) {
					if(perk instanceof NukePerk) {
						perk.run(player, zp.getArena());
					}
				}
			}
			event.setCancelled(true);
			item.remove();
		} else if(item.hasMetadata("DP")) {
			ZombiePlayer zp = ZombiePlayer.get(event.getPlayer());
			if(zp.getArena() != null) {
				zp.getArena().sendMessage(Main.getMsg().getString("ARENA_PERK.DOUBLE_POINTS"));
				zp.getArena().sendTitle(Main.getMsg().getString("ARENA_ACTIONBAR.DOUBLE_POINTS"), 20, 20, 60);
				
				for(Perk perk : Perk.perks) {
					if(perk instanceof DoublePoints) {
						perk.run(player, zp.getArena());
					}
				}
			}
			event.setCancelled(true);
			item.remove();
		} else if(item.hasMetadata("IK")) {
			ZombiePlayer zp = ZombiePlayer.get(event.getPlayer());
			if(zp.getArena() != null) {
				zp.getArena().sendMessage(Main.getMsg().getString("ARENA_PERK.INSTA_KILL"));
				zp.getArena().sendTitle(Main.getMsg().getString("ARENA_ACTIONBAR.DOUBLE_POINTS"), 20, 20, 60);
				
				for(Perk perk : Perk.perks) {
					if(perk instanceof InstaKill) {
						perk.run(player, zp.getArena());
					}
				}
			}
			event.setCancelled(true);
			item.remove();
		} else if(item.hasMetadata("INCINERATE")) {
			ZombiePlayer zp = ZombiePlayer.get(event.getPlayer());
			if(zp.getArena() != null) {
				zp.getArena().sendMessage(Main.getMsg().getString("ARENA_PERK.INCINERATE"));
				zp.getArena().sendTitle(Main.getMsg().getString("ARENA_ACTIONBAR.INCINERATE"), 20, 20, 60);
				
				for(Perk perk : Perk.perks) {
					if(perk instanceof Incinerate) {
						perk.run(player, zp.getArena());
					}
				}
			}
			event.setCancelled(true);
			item.remove();
		} else if(item.hasMetadata("GM")) {
			ZombiePlayer zp = ZombiePlayer.get(event.getPlayer());
			if(zp.getArena() != null) {
				zp.getArena().sendMessage(Main.getMsg().getString("ARENA_PERK.GOD_MODE"));
				zp.getArena().sendTitle(Main.getMsg().getString("ARENA_ACTIONBAR.GOD_MODE"), 20, 20, 60);
				
				for(Perk perk : Perk.perks) {
					if(perk instanceof Godmode) {
						perk.run(player, zp.getArena());
					}
				}
			}
			event.setCancelled(true);
			item.remove();
		}
	}
	
}
