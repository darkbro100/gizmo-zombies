package me.mario.zs.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.signs.LobbySign;
import me.mario.zs.signs.ReviveSign;
import me.mario.zs.util.ChatUtil;
import net.md_5.bungee.api.ChatColor;

public class BlockBreak implements Listener {
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		
		if (ReviveSign.getSign(e.getBlock().getLocation()) != null) {
			e.setCancelled(true);
			ReviveSign rs = ReviveSign.getSign(e.getBlock().getLocation());
			ZombiePlayer dead = ZombiePlayer.get(Bukkit.getPlayer(rs.getDeadPlayer()));
			
			block.setType(rs.getBlockBefore());
			dead.getPlayer().teleport(rs.getLocation());
			dead.setDead(false);
		}
		
		if (LobbySign.getSign(e.getBlock().getLocation()) != null) {
			//chest for perms
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);
			ReviveSign.signs.remove(LobbySign.getSign(e.getBlock().getLocation()));
			ChatUtil.sendMessage(e.getPlayer(), ChatColor.GREEN + "Successfully removed lobby sign!");
		}
	}
}
