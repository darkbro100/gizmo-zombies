package me.mario.zs.listeners;



import java.util.Random;

import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

public class EntityShootBow implements Listener {

	@EventHandler
	public void onEntityShoot(EntityShootBowEvent event) {
		if(event.getEntity() instanceof Skeleton) {
			if(((new Random().nextInt(10) + 1) == 1)) {
				event.getProjectile().setFireTicks(Integer.MAX_VALUE);
			}
		}
	}
}
