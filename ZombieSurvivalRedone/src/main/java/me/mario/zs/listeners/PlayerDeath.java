package me.mario.zs.listeners;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.signs.ReviveSign;
import me.mario.zs.util.LocUtil;

public class PlayerDeath implements Listener {
	
	@EventHandler
	public void onDamage(PlayerDeathEvent e) {
		e.setDeathMessage(null);
		if (!(e.getEntity() instanceof Player))
			return;
		Player player = (Player) e.getEntity();
		ZombiePlayer zp = ZombiePlayer.get(player);
		
		if (ArenaManager.getArena(player) == null)
			return;
		if (zp.isDead())
			return;
		
		e.getDrops().clear();
		new ReviveSign(player.getName(), player.getLocation());
		
		boolean allDead = true;
		for(String uuid : zp.getArena().getPlayersInGame()) {
			ZombiePlayer loop = ZombiePlayer.get(Bukkit.getPlayer(UUID.fromString(uuid)));
			if(loop.getPlayer().getName().equalsIgnoreCase(zp.getPlayer().getName())) continue;
			if(!loop.isDead()) allDead = false;
		}
		
		zp.getArena().sendMessage(Main.getMsg().getString("ARENA_MESSAGES.PLAYER_DIED").replace("%player%", e.getEntity().getName()).replace("%coords%", LocUtil.locToFriendlyString(e.getEntity().getLocation())));
		zp.getArena().sendActionBar(Main.getMsg().getString("ARENA_ACTIONBAR.PLAYER_DIED").replace("%player%", e.getEntity().getName()));
		e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation()).setFireTicks(0);
		
		if(allDead) {
			ArenaManager.endArena(zp.getArena());
			return;
		}
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				zp.setDead(true);
				player.spigot().respawn();
			}
		}, 2L);
	}
}
