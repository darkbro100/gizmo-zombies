package me.mario.zs.listeners;

import me.mario.zs.Main;
import me.mario.zs.ZombieItem;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.perks.DoublePoints;
import me.mario.zs.perks.Perk;
import me.mario.zs.perks.PerkManager;
import me.mario.zs.util.LocUtil;
import me.mario.zs.util.TitleUtil;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.ArrayList;

public class EntityDeath implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            e.getDrops().clear();
        }

        if (e.getEntity().getKiller() == null) {
            if (ArenaManager.getArena(e.getEntity()) != null) {
                Arena arena = ArenaManager.getArena(e.getEntity());
                ArrayList<Entity> toRemove = new ArrayList<Entity>();
                for (Entity ent : arena.getZombiesSpawned()) {
                    if (ent.getEntityId() == e.getEntity().getEntityId()) {
                        toRemove.add(ent);
                    }
                }
                arena.getZombiesSpawned().removeAll(toRemove);

                if (arena.getMobsSpawned() >= arena.getMaxMobs() && arena.getZombiesSpawned().size() <= 0) {
//					System.out.println("Ended wave");
                    ArenaManager.waveEnd(arena);
                }
            }
            return;
        }

        if (!(e.getEntity().getKiller() instanceof Player)) {
            if (ArenaManager.getArena(e.getEntity()) != null) {
                Arena arena = ArenaManager.getArena(e.getEntity());
                ArrayList<Entity> toRemove = new ArrayList<Entity>();
                for (Entity ent : arena.getZombiesSpawned()) {
                    if (ent.getEntityId() == e.getEntity().getEntityId()) {
                        toRemove.add(ent);
                    }
                }
                arena.getZombiesSpawned().removeAll(toRemove);

                if (arena.getMobsSpawned() >= arena.getMaxMobs() && arena.getZombiesSpawned().size() <= 0) {
//					System.out.println("Ended wave");
                    ArenaManager.waveEnd(arena);
                }
            }
            return;
        }
        if (e.getEntity() instanceof Giant || e.getEntity() instanceof Zombie || e.getEntity() instanceof Wolf || e.getEntity() instanceof Skeleton) {
            Arena arena = ArenaManager.getArena(e.getEntity());
            boolean dropped = PerkManager.dropPerk(arena, e.getEntity().getLocation());

            if (arena == null) return;

            if (!arena.isStarted()) return;
            ArrayList<Entity> toRemove = new ArrayList<Entity>();
            for (Entity ent : arena.getZombiesSpawned()) {
                if (ent.getEntityId() == e.getEntity().getEntityId()) {
                    toRemove.add(ent);
                }
            }
            arena.getZombiesSpawned().removeAll(toRemove);

            boolean doublePointsActive = false;
            for (Perk perk : arena.getActiveParks()) {
                if (perk instanceof DoublePoints) {
                    doublePointsActive = true;
                    break;
                }
            }

            if (dropped && !(arena.getCurrentWave() >= arena.getMaxWaves())) {
                arena.sendMessage(Main.getMsg().getString("ARENA_PERK.PERK_DROPPED").replace("%coords%", LocUtil.locToFriendlyString(e.getEntity().getLocation())));
                arena.sendActionBar(Main.getMsg().getString("ARENA_ACTIONBAR.PERK_DROPPED").replace("%coords%", LocUtil.locToFriendlyString(e.getEntity().getLocation())));
            }

            if (arena.getMobsSpawned() >= arena.getMaxMobs() && arena.getZombiesSpawned().size() <= 0) {
//					System.out.println("Ended wave");
                ArenaManager.waveEnd(arena);
            }

            int earned = Main.moneyPerKill;
            if (doublePointsActive) earned *= 2;

            Main.econ.depositPlayer(e.getEntity().getKiller(), earned);
            //sends annoying chat //ChatUtil.sendMessage(e.getEntity().getKiller(), Main.getMsg().getString("ARENA_MESSAGES.MOB_SLAINED").replace("%amount%", earned + ""));
            TitleUtil.sendActionBar(Main.getMsg().getString("ARENA_ACTIONBAR.MOB_SLAINED").replace("%amount%", earned + ""), e.getEntity().getKiller());
            // Percentages are already handled onEnable
            ZombieItem toDrop = Main.drops.select();

            if (toDrop != null && toDrop.getItem() != null && toDrop.getItem().getType() != Material.AIR && toDrop.getItem().getType() != Material.CAVE_AIR) {
                e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), toDrop.getItem());
            }
        }
    }
}
