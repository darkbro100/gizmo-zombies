package me.mario.zs.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.mario.zs.ZombiePlayer;

public class Join implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		new ZombiePlayer(e.getPlayer());
		e.getPlayer().teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		e.getPlayer().setGameMode(GameMode.SURVIVAL);
	}
}
