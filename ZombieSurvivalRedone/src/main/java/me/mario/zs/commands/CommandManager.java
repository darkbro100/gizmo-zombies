package me.mario.zs.commands;

import me.mario.zs.Main;
import me.mario.zs.arena.Arena;
import me.mario.zs.commands.zombie.*;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.MCStringUtil;
import org.antlr.v4.runtime.misc.NotNull;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CommandManager implements CommandExecutor, TabCompleter {

    public static ArrayList<ZSCommand> commands = new ArrayList<ZSCommand>();

    private List<String> mappedCommands = null;

    private Main plugin;

    public CommandManager(Main plugin) {
        this.plugin = plugin;

        commands.add(new AddZombieSpawn());
        commands.add(new CreateArenaCommand());
        commands.add(new JoinCommand());
        commands.add(new SetDoor());
        commands.add(new SetSpawnLocation());
        commands.add(new SetLobbySign());
        commands.add(new Leave());
        commands.add(new EditArena());
    }

    public Main plugin() {
        return plugin;
    }

    public List<String> mapped() {
        if (mappedCommands == null) {
            mappedCommands = commands.stream().map(ZSCommand::name).collect(Collectors.toList());
        }

        return mappedCommands;
    }

    public ZSCommand find(String name) {
        ZSCommand found = null;
        for (ZSCommand toFind : commands) {
            if (toFind.name().equalsIgnoreCase(name)) {
                found = toFind;
                break;
            }
        }

        return found;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("zs")) {
            if (args.length == 0) {
                for (ZSCommand zscmd : commands) {
                    ChatUtil.sendMessage((Player) sender, "&3" + zscmd.syntax() + " &f- &3" + zscmd.description());
                }
                return true;
            }
            ZSCommand found = find(args[0]);

            if (found == null) {
                ChatUtil.sendMessage((Player) sender, "&cUnknown command. Type /zs for a list of commands");
                return true;
            }

            ArrayList<String> argslist = new ArrayList<String>();

            for (String s : args) {
                argslist.add(s);
            }

            argslist.remove(0);

            String[] newargs = argslist.toArray(new String[argslist.size()]);

            found.run((Player) sender, newargs);
        } else if (sender instanceof Player && cmd.getName().equalsIgnoreCase("join")) {
            ZSCommand join = find("join");
            join.run(((Player) sender), args);
        }
        return true;
    }

    @Override
    public @Nullable
    List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if (command.getName().equalsIgnoreCase("join")) {
            return mapList(args.length > 0 ? args[0] : "");
        }

        if (args.length > 0) {
            String arg = args[0].toLowerCase();
            switch (arg) {
                case "setlobbysign", "join", "editarena" -> {
                    return mapList(args.length > 1 ? args[1] : "");
                }
            }
        }

        if (args.length <= 1) {
            return MCStringUtil.tabFilter(mapped(), args.length > 0 ? args[0] : "");
        }

        return MCStringUtil.tabFilter(players(), args[args.length - 1]);
    }

    private List<String> players() {
        return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
    }

    private List<String> mapList(String prefix) {
        LinkedList<String> results = new LinkedList<>();

        prefix = prefix.trim().toLowerCase();

        synchronized (Arena.arenas) {
            for (Arena a : Arena.arenas) {
                String code = a.getName();

                if (code.toLowerCase().startsWith(prefix))
                    results.add(code);
            }
        }

        return results;
    }

}
