package me.mario.zs.commands;

import org.bukkit.entity.Player;

public abstract class ZSCommand {

	public abstract String name();
	public abstract String description();
	public abstract String syntax();
	public abstract String permission();
	public abstract void run(Player sender, String[] args);
	
}
