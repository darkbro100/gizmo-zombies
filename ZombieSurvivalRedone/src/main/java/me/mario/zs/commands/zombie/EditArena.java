package me.mario.zs.commands.zombie;

import org.bukkit.entity.Player;

import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;

public class EditArena extends ZSCommand {

	@Override
	public String name() {
		return "editarena";
	}

	@Override
	public String description() {
		return "edit an arena";
	}

	@Override
	public String syntax() {
		return "/zs editarena <arena> [value] [newValue]";
	}

	@Override
	public String permission() {
		return "gz.editarena";
	}

	@Override
	public void run(Player sender, String[] args) {
		if (args.length == 0) {
			ChatUtil.sendMessage(sender, syntax());
			return;
		}

		if (args.length < 3) {
			Arena arena = ArenaManager.getArena(args[0]);
			if (arena == null) {
				ChatUtil.sendMessage(sender, "Arena does not exist");
				return;
			}
			ZombiePlayer.get(sender).setBuildingArena(arena);
			ZombiePlayer.get(sender).setCreatingArena(true);
			ChatUtil.sendMessage(sender, "&aNow editing arena: " + args[0]);
		} else {
			Arena arena = ArenaManager.getArena(args[0]);
			if (args[1].equalsIgnoreCase("name") || args[1].equalsIgnoreCase("healthMulti") || args[1].equalsIgnoreCase("spawnMulti")
					|| args[1].equalsIgnoreCase("maxPlayers") || args[1].equalsIgnoreCase("maxWaves")) {
				switch(args[1].toLowerCase()) {
				case "name":
					Main.getFileHandler().deleteArenaFile(arena);
					arena.setName(args[2]);
					break;
				case "healthmulti":
					arena.setHealthMultiplier(Double.parseDouble(args[2]));
					break;
				case "spawnmulti":
					arena.setSpawnMulitplier(Double.parseDouble(args[2]));
					break;
				case "maxplayers":
					arena.setMaxPlayers(Integer.parseInt(args[2]));
					break;
				case "maxwaves":
					arena.setMaxWaves(Integer.parseInt(args[2]));
					break;
				}
				ChatUtil.sendMessage(sender, "updated value: " + args[1] + " to: " + args[2]);
			} else {
				ChatUtil.sendMessage(sender, "Available values: <name> <healthMulti> <spawnMulti> <maxPlayers> <maxWaves>");
				return;
			}
		}
	}

}
