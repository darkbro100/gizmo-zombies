package me.mario.zs.commands.zombie;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.LocUtil;

public class AddZombieSpawn extends ZSCommand {

	@Override
	public String name() {
		return "setzombiespawn";
	}
	@Override
	public String description() {
		return "set spawn location for zombies";
	}

	@Override
	public String syntax() {
		return "/zs setzombiespawn <list>/<remove>/<wave>";
	}

	@Override
	public String permission() {
		return "";
	}

	@Override
	public void run(Player sender, String[] args) {
		if(ZombiePlayer.get(sender).getBuildingArena() == null) {
			ChatUtil.sendMessage(sender, "&cYou currently aren't building an arena");
			return;
		}
		
		if(args.length == 0) {
			ChatUtil.sendMessage(sender, "&c" + syntax());
			return;
		}
		
		Arena arena = ZombiePlayer.get(sender).getBuildingArena();
		if(args[0].equalsIgnoreCase("list")) {
			for(Entry<Location, Integer> entry : arena.getWaveSpawns().entrySet()) {
				ChatUtil.sendMessage(sender, LocUtil.locToString(entry.getKey()) + " : " + entry.getValue());
			}
			return;
		}
		if(args[0].equalsIgnoreCase("remove") && args.length > 1) {
			try {
				int wave = Integer.parseInt(args[1]);
				Set<Entry<Location, Integer>> toRemove = new HashSet<Entry<Location, Integer>>();
				
				for(Entry<Location, Integer> entry : arena.getWaveSpawns().entrySet()) {
					if(entry.getValue() == wave) {
						toRemove.add(entry);
					}
				}
				arena.getWaveSpawns().entrySet().removeAll(toRemove);
				ChatUtil.sendMessage(sender, "removed all spawns @ wave" + wave);
				return;
			}catch(Exception e) {
				ChatUtil.sendMessage(sender, "&cpls write real number");
				return;
			}
		}
		
		HashMap<Location, Integer> zombieSpawns = arena.getWaveSpawns();
		zombieSpawns.put(sender.getLocation(), Integer.parseInt(args[0]));
		arena.setWaveSpawns(zombieSpawns);
		ChatUtil.sendMessage(sender, "&aAdded new zombie spawn location for wave number: " + args[0]);
	}
	
}
