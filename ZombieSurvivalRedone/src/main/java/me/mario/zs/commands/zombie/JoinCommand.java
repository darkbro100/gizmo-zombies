package me.mario.zs.commands.zombie;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;

public class JoinCommand extends ZSCommand {

	@Override
	public String name() {
		return "join";
	}
	@Override
	public String description() {
		return "Join an arena";
	}

	@Override
	public String syntax() {
		return "/zs join <arena>";
	}

	@Override
	public String permission() {
		return "";
	}

	@Override
	public void run(Player sender, String[] args) {
		if (args.length == 0) {
			ChatUtil.sendMessage(sender, ChatColor.GREEN + "Join an arena using " + syntax());
			ChatUtil.sendMessage(sender, ChatColor.GREEN + "Joinable arenas...");
			sender.sendMessage(" ");
			ArrayList<String> available = new ArrayList<String>();
			for (Arena a : Arena.arenas) {
				if(a.getPlayersInGame().size() >= a.getMaxPlayers()) continue;
				available.add(a.getName());
			}
			ChatUtil.sendMessage(sender, ChatColor.BLUE + available.toString());
			return;
		}
		Arena arena = ArenaManager.getArena(args[0]);
		if (arena == null) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "Cannot find map " + args[0] + "!");
			ChatUtil.sendMessage(sender, ChatColor.GREEN + "Joinable arenas...");
			sender.sendMessage(" ");
			ArrayList<String> available = new ArrayList<String>();
			for (Arena a : Arena.arenas) {
				if(a.getPlayersInGame().size() >= a.getMaxPlayers()) continue;
				available.add(a.getName());
			}
			ChatUtil.sendMessage(sender, ChatColor.BLUE + available.toString());
			return;
		}
		
		if (arena.isDonatorMap()) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "You must be a donator to play this map!");
			return;
		}
		
		if (arena.getPlayersInGame().size() >= arena.getMaxPlayers()) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "This map is currently full! Try a different one!");
			return;
		}
		
		if(ZombiePlayer.get(sender).getArena() != null) {
			ChatUtil.sendMessage(sender, "&cYou are already in a game!");
			return;
		}
		
		ArenaManager.joinArena(sender, arena);
	}
	
}
