package me.mario.zs.commands.zombie;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;

public class Leave extends ZSCommand {

	@Override
	public String name() {
		return "leave";
	}
	@Override
	public String description() {
		return "Leave an arena";
	}

	@Override
	public String syntax() {
		return "/zs leave";
	}

	@Override
	public String permission() {
		return "";
	}

	@Override
	public void run(Player sender, String[] args) {
		ZombiePlayer zp = ZombiePlayer.get(sender);
		if (zp.getArena() == null) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "You are currently not in a game!");
			return;
		}
		Arena arena = zp.getArena();
		
		ArenaManager.leaveArena(sender, arena);
	}
	
}
