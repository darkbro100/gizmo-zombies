package me.mario.zs.commands.zombie;

import org.bukkit.entity.Player;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.arena.ArenaManager;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;

public class CreateArenaCommand extends ZSCommand {

	@Override
	public String name() {
		return "createarena";
	}

	@Override
	public String description() {
		return "Create an arena";
	}

	@Override
	public String syntax() {
		return "/zs createarena <name> <healthMulti> <spawnMulti> <maxPlayers> <maxWaves>";
	}

	@Override
	public String permission() {
		return "gz.createarena";
	}

	@Override
	public void run(Player sender, String[] args) {
		if (args.length < 5) {
			ChatUtil.sendMessage(sender, "&c" + syntax());
			return;
		}
		if (!sender.hasPermission(permission()))
			return;

		String name = args[0];
		int healthMultiplier = Integer.parseInt(args[1]);
		double spawnMultiplier = Double.parseDouble(args[2]);
		int maxPlayers = Integer.parseInt(args[3]);
		int maxWaves = Integer.parseInt(args[4]);
		
		if (ArenaManager.getArena(name) != null) {
			ChatUtil.sendMessage(sender, "&cAn arena with that name already exists");
			return;
		}

		Arena arena = new Arena(name);
		arena.setHealthMultiplier(healthMultiplier);
		arena.setSpawnMulitplier(spawnMultiplier);
		arena.setMaxPlayers(maxPlayers);
		arena.setMaxWaves(maxWaves);
		
		ZombiePlayer.get((Player) sender).setCreatingArena(true);
		ZombiePlayer.get((Player) sender).setBuildingArena(arena);
		ChatUtil.sendMessage(sender,
				"&aYou are now creating an area. Set a spawn, zombie spawn, max waves, health multiplier, spawn multiplier, and max players");
	}
}
