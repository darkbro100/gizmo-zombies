package me.mario.zs.commands.zombie;

import org.bukkit.entity.Player;

import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;

public class SetSpawnLocation extends ZSCommand {

	@Override
	public String name() {
		return "setspawn";
	}
	@Override
	public String description() {
		return "set spawn location for players";
	}

	@Override
	public String syntax() {
		return "/zs setspawn";
	}

	@Override
	public String permission() {
		return "";
	}

	@Override
	public void run(Player sender, String[] args) {
		if(ZombiePlayer.get(sender).getBuildingArena() == null) {
			ChatUtil.sendMessage(sender, "&cYou currently aren't building an arena");
			return;
		}
		
		Arena arena = ZombiePlayer.get(sender).getBuildingArena();
		arena.setSpawnLocation(sender.getLocation());
		ChatUtil.sendMessage(sender, "&aUpdated spawn location");
	}

}
