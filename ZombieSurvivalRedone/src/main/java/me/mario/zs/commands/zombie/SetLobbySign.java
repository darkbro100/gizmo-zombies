package me.mario.zs.commands.zombie;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import me.mario.zs.arena.ArenaManager;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.signs.LobbySign;
import me.mario.zs.util.ChatUtil;

public class SetLobbySign extends ZSCommand {

	@Override
	public String name() {
		return "setlobbysign";
	}
	@Override
	public String description() {
		return "Set lobby sign";
	}

	@Override
	public String syntax() {
		return "/zs setlobbysign <arena>";
	}

	@Override
	public String permission() {
		return "zs.createsign";
	}

	@Override
	public void run(Player sender, String[] args) {
		if (args.length == 0) {
			ChatUtil.sendMessage(sender, ChatColor.RED + syntax());
			return;
		}
		
		if(!sender.hasPermission(permission())) return;
		
		Block block = sender.getTargetBlock((Set<Material>) null, 100);
		if (!(block.getState() instanceof Sign)) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "You must be looking at a sign!");
			return;
		}
		
		if (LobbySign.getSign(block.getLocation()) != null) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "A lobby sign already exists at this location!");
			return;
		}
		
		if (ArenaManager.getArena(args[0]) == null) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "Cannot find arena " + args[0] + "!");
			return;
		}
		
		new LobbySign(ArenaManager.getArena(args[0]), block.getLocation());
		ChatUtil.sendMessage(sender, ChatColor.GREEN + "Successfully created lobby sign for arena " + args[0] + "!");
	}
	
}
