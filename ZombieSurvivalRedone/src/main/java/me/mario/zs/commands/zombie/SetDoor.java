package me.mario.zs.commands.zombie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map.Entry;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.SessionManager;
import com.sk89q.worldedit.session.SessionOwner;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;


import me.mario.zs.Main;
import me.mario.zs.ZombiePlayer;
import me.mario.zs.arena.Arena;
import me.mario.zs.commands.ZSCommand;
import me.mario.zs.util.ChatUtil;
import me.mario.zs.util.LocUtil;

public class SetDoor extends ZSCommand {

	@Override
	public String name() {
		return "setdoor";
	}
	@Override
	public String description() {
		return "Set door location";
	}

	@Override
	public String syntax() {
		return "/zs setdoor <list>/<remove>/<wave>";
	}

	@Override
	public String permission() {
		return "";
	}

	@Override
	public void run(Player sender, String[] args) {
		if(ZombiePlayer.get(sender).getBuildingArena() == null) {
			ChatUtil.sendMessage(sender, "&cYou currently aren't building an arena");
			return;
		}
		
		if(args.length == 0) {
			ChatUtil.sendMessage(sender, "&c" + syntax());
			return;
		}
		
		Arena arena = ZombiePlayer.get(sender).getBuildingArena();
		if(args[0].equalsIgnoreCase("list")) {
			for(Entry<ArrayList<Location>, Integer> entry : arena.getDoorSpawns().entrySet()) {
				for(Location loc : entry.getKey()) {
					ChatUtil.sendMessage(sender, LocUtil.locToString(loc) + " : " + entry.getValue());
				}
			}
			return;
		}
		if(args[0].equalsIgnoreCase("remove") && args.length > 1) {
			try {
				int wave = Integer.parseInt(args[1]);
				Set<Entry<ArrayList<Location>, Integer>> toRemove = new HashSet<Entry<ArrayList<Location>, Integer>>();
				
				for(Entry<ArrayList<Location>, Integer> entry : arena.getDoorSpawns().entrySet()) {
					if(entry.getValue() == wave) {
						toRemove.add(entry);
					}
				}
				arena.getWaveSpawns().entrySet().removeAll(toRemove);
				ChatUtil.sendMessage(sender, "removed all doors @ wave" + wave);
				return;
			}catch(Exception e) {
				ChatUtil.sendMessage(sender, "&cpls write real number");
				return;
			}
		}


		SessionManager manager = WorldEdit.getInstance().getSessionManager();
		LocalSession localSession = manager.get(BukkitAdapter.adapt(sender));
		if (localSession == null) {
			return;
		}
		Region door = null;
		try {
			door = localSession.getSelection(localSession.getSelectionWorld());
		} catch (IncompleteRegionException e) {
			e.printStackTrace();
		}
		if (door == null) {
			ChatUtil.sendMessage(sender, ChatColor.RED + "You must make a selection first!");
			return;
		}
		
		HashMap<ArrayList<Location>, Integer> doorSelections = arena.getDoorSpawns();
		int wave = Integer.parseInt(args[0]);
		ArrayList<Location> loc = new ArrayList<Location>();
		Location door1 = new Location(sender.getWorld(), door.getMaximumPoint().getX(), door.getMaximumPoint().getY(), door.getMaximumPoint().getZ());
		Location door2 = new Location(sender.getWorld(), door.getMinimumPoint().getX(), door.getMinimumPoint().getY(), door.getMinimumPoint().getZ());
		loc.add(door1);
		loc.add(door2);
		doorSelections.put(loc, wave);
		arena.setDoorSpawns(doorSelections);
		ChatUtil.sendMessage(sender, "&aAdded new door for wave number: " + args[0]);
	}
	
}
